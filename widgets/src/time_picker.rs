use std::f32::consts::PI;
use eframe::emath::vec2;
use eframe::epaint::{Color32, Stroke};
use egui::{Align2, Button, Pos2, Response, Sense, Ui};
use crate::TimePickerMode;

pub struct TimePicker {
    width: f32,
    hour_hand_color: Color32,
    minute_hand_color: Color32,
    preview_color: Color32,
}

impl Default for TimePicker {
    fn default() -> Self {
        Self {
            width: 200.0,
            hour_hand_color: Color32::BLUE,
            minute_hand_color: Color32::BLUE,
            preview_color: Color32::from_rgba_premultiplied(0, 0, 255, 100),
        }
    }
}

impl TimePicker {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn width(mut self, width: f32) -> Self {
        self.width = width;
        self
    }

    pub fn hour_hand_color(mut self, color: Color32) -> Self {
        self.hour_hand_color = color;
        self
    }

    pub fn minute_hand_color(mut self, color: Color32) -> Self {
        self.minute_hand_color = color;
        self
    }

    pub fn preview_color(mut self, color: Color32) -> Self {
        self.preview_color = color;
        self
    }

    fn draw_clock_face(&self, ui: &mut Ui, center: Pos2, radius: f32) {
        // Draw main circle
        ui.painter().circle_stroke(
            center,
            radius,
            Stroke::new(2.0, Color32::GRAY),
        );

        // Draw hour marks
        for i in 0..12 {
            let angle = i as f32 * PI / 6.0 - PI / 2.0;
            let outer_pos = center + vec2(angle.cos(), angle.sin()) * radius;
            let inner_pos = center + vec2(angle.cos(), angle.sin()) * (radius - 10.0);
            ui.painter().line_segment(
                [outer_pos, inner_pos],
                Stroke::new(1.0, Color32::GRAY),
            );
        }
    }

    fn draw_numbers(&self, ui: &mut Ui, center: Pos2, radius: f32, mode: TimePickerMode) {
        let font_id = egui::FontId::proportional(14.0);
        let text_color = Color32::GRAY;
        let number_radius = radius - 25.0;  // Inset the numbers from the edge

        for i in 0..12 {
            let angle = i as f32 * PI / 6.0 - PI / 2.0;
            let pos = center + vec2(angle.cos(), angle.sin()) * number_radius;

            let number = match mode {
                TimePickerMode::Hour => {
                    if i == 0 { "12" } else { &format!("{}", i) }
                }
                TimePickerMode::Minute => {
                    &format!("{}", (i * 5) % 60)
                }
            };

            ui.painter().text(
                pos,
                Align2::CENTER_CENTER,
                number,
                font_id.clone(),
                text_color,
            );
        }
    }

    fn calculate_time_from_angle(&self, angle_deg: f32, mode: TimePickerMode) -> u8 {
        match mode {
            TimePickerMode::Hour => {
                let hour = ((angle_deg / 30.0).round() as u8) % 12;
                if hour == 0 { 12 } else { hour }
            },
            TimePickerMode::Minute => ((angle_deg / 6.0).round() as u8) % 60,
        }
    }

    fn handle_interaction(
        &self,
        ui: &mut Ui,
        response: &Response,
        center: Pos2,
        radius: f32,
        hour: &mut u8,
        minute: &mut u8,
        mode: &mut TimePickerMode,
        is_pm: bool,
    ) {
        if let Some(pos) = response.hover_pos() {
            let vec = pos - center;
            // Only handle interactions within the clock face
            if vec.length() <= radius {
                let angle = vec.y.atan2(vec.x);
                let angle_deg = (angle.to_degrees() + 450.0) % 360.0;

                let hover_time = self.calculate_time_from_angle(angle_deg, *mode);
                self.draw_preview_hand(ui, center, radius, hover_time, *mode);

                // Set time on click
                if response.clicked() {
                    match mode {
                        TimePickerMode::Hour => {
                            // Convert 12-hour selection to 24-hour format
                            *hour = if hover_time == 12 {
                                if is_pm { 12 } else { 0 }
                            } else if is_pm {
                                hover_time + 12
                            } else {
                                hover_time
                            };
                            *mode = TimePickerMode::Minute;
                        }
                        TimePickerMode::Minute => {
                            *minute = hover_time;
                            *mode = TimePickerMode::Hour;
                        }
                    }
                }
            }
        }
    }

    fn draw_preview_hand(&self, ui: &mut Ui, center: Pos2, radius: f32, value: u8, mode: TimePickerMode) {
        let angle = match mode {
            TimePickerMode::Hour => (value as f32 * PI / 6.0) - (PI / 2.0),
            TimePickerMode::Minute => (value as f32 * PI / 30.0) - (PI / 2.0),
        };

        let length = match mode {
            TimePickerMode::Hour => radius * 0.5,
            TimePickerMode::Minute => radius * 0.7,
        };

        let end_pos = center + vec2(angle.cos(), angle.sin()) * length;
        ui.painter().line_segment(
            [center, end_pos],
            Stroke::new(2.0, self.preview_color),
        );
    }

    fn draw_current_hand(&self, ui: &mut Ui, center: Pos2, radius: f32, value: u8, mode: TimePickerMode) {
        let (angle, length, color) = match mode {
            TimePickerMode::Hour => (
                (value as f32 * PI / 6.0) - (PI / 2.0),
                radius * 0.5,
                self.hour_hand_color,
            ),
            TimePickerMode::Minute => (
                (value as f32 * PI / 30.0) - (PI / 2.0),
                radius * 0.7,
                self.minute_hand_color,
            ),
        };

        let end_pos = center + vec2(angle.cos(), angle.sin()) * length;
        ui.painter().line_segment(
            [center, end_pos],
            Stroke::new(if mode == TimePickerMode::Hour { 3.0 } else { 2.0 }, color),
        );
    }

    fn draw_time_display(&self, ui: &mut Ui, rect: egui::Rect, diameter: f32, hour: u8, minute: u8) -> Response {
        let display_hour = match hour {
            0 => "12",
            13..=23 => &format!("{}", hour - 12),
            _ => &format!("{}", hour),
        };

        // Draw the time numbers
        ui.painter().text(
            rect.min + vec2(diameter / 2.0 - 20.0, diameter + 10.0),
            Align2::CENTER_TOP,
            format!("{:0>2}:{:02}", display_hour, minute),
            egui::FontId::proportional(20.0),
            ui.visuals().text_color(),
        );

        // Create a small button for AM/PM that looks like part of the time display
        let is_pm = hour >= 12;
        let button_size = vec2(35.0, 25.0);
        let button_pos = rect.min + vec2(diameter / 2.0 + 25.0, diameter + 8.0);
        let button_rect = egui::Rect::from_min_size(button_pos, button_size);

        ui.put(button_rect, Button::new(if is_pm { "PM" } else { "AM" }))
    }

    pub fn show(&mut self, ui: &mut Ui, hour: &mut u8, minute: &mut u8, mode: &mut TimePickerMode) -> Response {
        // Ensure time values are in valid ranges
        *hour = *hour % 24;
        *minute = *minute % 60;

        let is_pm = *hour >= 12;

        // Convert 24h to 12h for display
        let display_hour = match *hour {
            0 => 12,
            13..=23 => *hour - 12,
            _ => *hour,
        };

        let available_size = ui.available_size();
        let diameter = available_size.x.min(available_size.y).min(self.width);
        let radius = diameter / 2.0;
        let center = ui.next_widget_position() + vec2(radius, radius);

        // Allocate space for both clock and text
        let total_height = diameter + 30.0; // Extra space for text below
        let (rect, response) = ui.allocate_exact_size(
            vec2(diameter, total_height),
            Sense::click(),
        );

        self.draw_clock_face(ui, center, radius);
        self.draw_numbers(ui, center, radius, *mode);

        self.handle_interaction(ui, &response, center, radius, hour, minute, mode, is_pm);

        // Draw the current hand
        self.draw_current_hand(ui, center, radius,
                               if *mode == TimePickerMode::Hour { display_hour } else { *minute },
                               *mode
        );

        // Center dot
        ui.painter().circle_filled(center, 4.0, Color32::DARK_GRAY);

        // If AM/PM was clicked, toggle the hour
        if self.draw_time_display(ui, rect, diameter, *hour, *minute).clicked() {
            *hour = match *hour {
                0 => 12,  // 12 AM -> 12 PM
                12 => 0,  // 12 PM -> 12 AM
                1..=11 => *hour + 12, // AM -> PM
                _ => *hour - 12,      // PM -> AM
            };
        }

        response
    }
}