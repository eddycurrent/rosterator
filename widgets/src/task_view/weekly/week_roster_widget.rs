use chrono::{Duration, NaiveDate};
use egui::{vec2, Align2, Color32, Rect, Response, Sense, Stroke, Ui};
use crate::task_view::weekly::TaskViewWeek;

pub struct WeekRosterWidget {
    width: f32,
    height: f32,
    current_date: NaiveDate,
    task_height: f32,
    day_header_height: f32,
    last_clicked_date: Option<NaiveDate>,
}

impl Default for WeekRosterWidget {
    fn default() -> Self {
        Self {
            width: 600.0,
            height: 400.0,
            current_date: chrono::Local::now().date_naive(),
            task_height: 30.0,
            day_header_height: 40.0,
            last_clicked_date: None,
        }
    }
}

impl WeekRosterWidget {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn width(mut self, width: f32) -> Self {
        self.width = width;
        self
    }

    pub fn height(mut self, height: f32) -> Self {
        self.height = height;
        self
    }

    // Returns the date that was clicked, if any
    pub fn clicked_date(&self) -> Option<NaiveDate> {
        self.last_clicked_date
    }

    fn get_week_dates(&self) -> Vec<NaiveDate> {
        (0..7)
            .map(|days| self.current_date + Duration::days(days as i64))
            .collect()
    }

    pub fn start_date(mut self, date: NaiveDate) -> Self {
        self.current_date = date;
        self.last_clicked_date = None;  // Reset clicked date when changing week
        self
    }

    fn draw_day_headers(&self, ui: &mut Ui, rect: Rect) -> Option<NaiveDate> {
        let dates = self.get_week_dates();
        let day_width = rect.width() / 7.0;
        let mut clicked_date = None;

        for (i, date) in dates.iter().enumerate() {
            let day_rect = Rect::from_min_size(
                rect.min + vec2(day_width * i as f32, 0.0),
                vec2(day_width, rect.height()),
            );

            // Handle clicks for the entire day column
            let day_response = ui.allocate_rect(day_rect, Sense::click());
            if day_response.clicked() {
                clicked_date = Some(*date);
            }

            // Draw day border
            ui.painter().rect_stroke(
                day_rect,
                0.0,
                Stroke::new(1.0, Color32::GRAY),
            );

            // Draw hover highlight
            if day_response.hovered() {
                ui.painter().rect_filled(
                    day_rect,
                    0.0,
                    Color32::from_white_alpha(10),
                );
            }

            let header_rect = Rect::from_min_size(
                day_rect.min,
                vec2(day_width, self.day_header_height),
            );

            // Draw day name
            ui.painter().text(
                header_rect.center_top() + vec2(0.0, 5.0),
                Align2::CENTER_TOP,
                date.format("%a").to_string(),
                egui::FontId::proportional(14.0),
                ui.visuals().text_color(),
            );

            // Draw day number
            ui.painter().text(
                header_rect.center() + vec2(0.0, 5.0),
                Align2::CENTER_CENTER,
                date.format("%d").to_string(),
                egui::FontId::proportional(16.0),
                ui.visuals().text_color(),
            );
        }

        clicked_date
    }

    fn draw_task(&self, ui: &mut Ui, task: &TaskViewWeek, rect: Rect, week_start: NaiveDate) {
        let day_width = rect.width() / 7.0;

        // Calculate task position and width
        let task_start_offset = if task.start_date < week_start {
            0.0
        } else {
            (task.start_date - week_start).num_days() as f32 * day_width
        };

        let days_in_week = {
            let week_end = week_start + Duration::days(6);
            let task_end = if task.end_date > week_end {
                week_end
            } else {
                task.end_date
            };
            let start = if task.start_date < week_start {
                week_start
            } else {
                task.start_date
            };
            (task_end - start).num_days() as f32 + 1.0
        };

        let task_width = days_in_week * day_width;

        // Draw task rectangle
        let task_rect = Rect::from_min_size(
            rect.min + vec2(task_start_offset, 0.0),
            vec2(task_width, self.task_height),
        );

        ui.painter().rect_filled(
            task_rect,
            4.0,
            task.color,
        );

        // Draw task title
        ui.painter().text(
            task_rect.left_center() + vec2(5.0, 0.0),
            Align2::LEFT_CENTER,
            &task.title,
            egui::FontId::proportional(14.0),
            Color32::WHITE,
        );
    }

    pub fn show(&mut self, ui: &mut Ui, tasks: &[TaskViewWeek]) -> Response {
        let (rect, response) = ui.allocate_exact_size(
            vec2(self.width, self.height),
            Sense::click_and_drag(),
        );

        // Draw background
        ui.painter().rect_filled(
            rect,
            0.0,
            ui.visuals().extreme_bg_color,
        );

        // Create clipping rect for the content
        let clip_rect = rect;
        ui.set_clip_rect(clip_rect);

        // Handle clicks and draw day headers
        if let Some(clicked) = self.draw_day_headers(ui, rect) {
            self.last_clicked_date = Some(clicked);
        } else if response.clicked() {
            // Reset if clicked outside a day
            self.last_clicked_date = None;
        }

        // Calculate tasks area
        let tasks_rect = Rect::from_min_size(
            rect.min + vec2(0.0, self.day_header_height),
            vec2(rect.width(), rect.height() - self.day_header_height),
        );

        // Draw tasks
        let week_start = self.get_week_dates()[0];
        for (i, task) in tasks.iter().enumerate() {
            let task_rect = Rect::from_min_size(
                tasks_rect.min + vec2(0.0, i as f32 * (self.task_height + 5.0)),
                vec2(tasks_rect.width(), self.task_height),
            );
            self.draw_task(ui, task, task_rect, week_start);
        }

        // Handle navigation
        if ui.input(|i| i.key_pressed(egui::Key::ArrowLeft)) {
            self.current_date = self.current_date - Duration::days(7);
            self.last_clicked_date = None;  // Reset clicked date on navigation
        }
        if ui.input(|i| i.key_pressed(egui::Key::ArrowRight)) {
            self.current_date = self.current_date + Duration::days(7);
            self.last_clicked_date = None;  // Reset clicked date on navigation
        }

        response
    }
}