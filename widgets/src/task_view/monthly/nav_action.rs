/// Navigation action for the calendar widget
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum NavAction {
    None,
    PrevMonth,
    NextMonth,
    PrevYear,
    NextYear,
}
