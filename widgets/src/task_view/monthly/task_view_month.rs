use chrono::NaiveDate;
use eframe::epaint::Color32;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct TaskViewMonth {
    pub id: u32,
    pub title: String,
    pub start_date: NaiveDate,
    pub end_date: NaiveDate,
    pub color: Color32,
}
