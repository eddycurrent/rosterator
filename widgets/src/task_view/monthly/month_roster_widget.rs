use std::collections::HashMap;
use chrono::{Datelike, Duration, NaiveDate, Weekday};
use egui::{vec2, Align2, Color32, Rect, Response, Sense, Shape, Stroke, Ui, Window};
use crate::task_view::monthly::{NavAction, TaskViewMonth};

pub struct MonthRosterWidget {
    width: f32,
    height: f32,
    current_date: NaiveDate,
    task_height: f32,
    day_header_height: f32,
    last_clicked_date: Option<NaiveDate>,
    last_clicked_task: Option<u32>,
    min_year: i32,
    max_year: i32,
    show_year_dropdown: bool,
    show_month_dropdown: bool,
    week_start_day: Weekday,
    popout_date: Option<NaiveDate>,
    tasks_per_cell: HashMap<NaiveDate, Vec<u32>>,
    /// Maximum number of tasks to show in a non-expanded calendar day cell
    max_visible_tasks: usize,
    popout_cell_rect: Option<Rect>,
}

impl Default for MonthRosterWidget {
    fn default() -> Self {
        Self {
            width: 800.0,
            height: 600.0,
            current_date: chrono::Local::now().date_naive(),
            task_height: 25.0,
            day_header_height: 30.0,
            last_clicked_date: None,
            last_clicked_task: None,
            min_year: chrono::Local::now().date_naive().year() - 10,
            max_year: chrono::Local::now().date_naive().year() + 10,
            show_year_dropdown: false,
            show_month_dropdown: false,
            week_start_day: Weekday::Sun,
            popout_date: None,
            tasks_per_cell: HashMap::new(),
            max_visible_tasks: 3,
            popout_cell_rect: None,
        }
    }
}

impl MonthRosterWidget {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn width(mut self, width: f32) -> Self {
        self.width = width;
        self
    }

    pub fn set_width(&mut self, width: f32) {
        self.width = width;
    }

    pub fn height(mut self, height: f32) -> Self {
        self.height = height;
        self
    }

    pub fn set_height(&mut self, height: f32) {
        self.height = height;
    }

    pub fn year_range(mut self, min_year: i32, max_year: i32) -> Self {
        self.min_year = min_year;
        self.max_year = max_year;
        self
    }

    pub fn set_year_range(&mut self, min_year: i32, max_year: i32) {
        self.min_year = min_year;
        self.max_year = max_year;
    }

    /// Get the current date being displayed in the calendar
    pub fn get_current_date(&self) -> NaiveDate {
        self.current_date
    }

    /// Get the date that was clicked by the user, if any
    pub fn clicked_date(&self) -> Option<NaiveDate> {
        self.last_clicked_date
    }

    pub fn set_start_date(&mut self, date: NaiveDate) {
        self.current_date = date;
        self.last_clicked_date = None;
    }

    pub fn week_start_day(mut self, day: Weekday) -> Self {
        self.week_start_day = day;
        self
    }

    pub fn set_week_start_day(&mut self, day: Weekday) {
        self.week_start_day = day;
    }

    pub fn get_week_start_day(&self) -> Weekday {
        self.week_start_day
    }

    /// Get the ID of the task that was clicked by the user, if any
    pub fn clicked_task(&self) -> Option<u32> {
        self.last_clicked_task
    }

    fn get_month_dates(&self) -> (Vec<Option<NaiveDate>>, usize) {
        // Get the first day of the month
        let first_day = NaiveDate::from_ymd_opt(
            self.current_date.year(),
            self.current_date.month(),
            1
        ).unwrap();

        // Get the last day of the month
        let last_day = if self.current_date.month() == 12 {
            NaiveDate::from_ymd_opt(self.current_date.year() + 1, 1, 1).unwrap() - Duration::days(1)
        } else {
            NaiveDate::from_ymd_opt(self.current_date.year(), self.current_date.month() + 1, 1).unwrap() - Duration::days(1)
        };

        // Calculate days from previous month to fill the first week
        // We need to find the day that's the configured start day of the week
        let start_day_offset = self.week_start_day.num_days_from_monday() as i64;
        let first_day_weekday = first_day.weekday().num_days_from_monday() as i64;

        // Calculate how many days to go back to reach the start day of the week
        let days_to_go_back = (7 + first_day_weekday - start_day_offset) % 7;

        let mut current_date = first_day - Duration::days(days_to_go_back);

        // Calculate the number of weeks needed
        let mut dates = Vec::new();
        let mut weeks = 0;

        // Continue until we've included the last day of the month
        // and completed a full week
        let mut days_in_week = 0;
        while current_date <= last_day || days_in_week % 7 != 0 {
            if current_date.month() == self.current_date.month() {
                dates.push(Some(current_date));
            } else {
                dates.push(None);
            }
            current_date = current_date + Duration::days(1);
            days_in_week += 1;

            if days_in_week % 7 == 0 {
                weeks += 1;
            }
        }

        // The above loop guarantees that we end with a complete week,
        // so we don't need additional padding

        (dates, weeks)
    }

    fn draw_month_header(&mut self, ui: &mut Ui, rect: Rect) -> (bool, NavAction) {
        let mut nav_result = (false, NavAction::None);

        // Create five sections for the header (prev year, prev month, title, next month, next year)
        let section_width = rect.width() / 5.0;

        // Prev year button
        let prev_year_rect = Rect::from_min_size(
            rect.min,
            vec2(section_width, rect.height()),
        );

        // Prev month button
        let prev_month_rect = Rect::from_min_size(
            rect.min + vec2(section_width, 0.0),
            vec2(section_width, rect.height()),
        );

        // Middle section for month/year text and dropdowns
        let title_rect = Rect::from_min_size(
            rect.min + vec2(section_width * 2.0, 0.0),
            vec2(section_width, rect.height()),
        );

        // Next month button
        let next_month_rect = Rect::from_min_size(
            rect.min + vec2(section_width * 3.0, 0.0),
            vec2(section_width, rect.height()),
        );

        // Next year button
        let next_year_rect = Rect::from_min_size(
            rect.min + vec2(section_width * 4.0, 0.0),
            vec2(section_width, rect.height()),
        );

        // Draw and handle prev year button
        let prev_year_response = ui.allocate_rect(prev_year_rect, Sense::click());
        if prev_year_response.hovered() {
            ui.painter().rect_filled(
                prev_year_rect,
                0.0,
                Color32::from_white_alpha(10),
            );
        }
        ui.painter().text(
            prev_year_rect.center(),
            Align2::CENTER_CENTER,
            "◀◀",
            egui::FontId::proportional(18.0),
            ui.visuals().text_color(),
        );
        if prev_year_response.clicked() {
            nav_result = (true, NavAction::PrevYear);
            return nav_result;
        }

        // Draw and handle prev month button
        let prev_month_response = ui.allocate_rect(prev_month_rect, Sense::click());
        if prev_month_response.hovered() {
            ui.painter().rect_filled(
                prev_month_rect,
                0.0,
                Color32::from_white_alpha(10),
            );
        }
        ui.painter().text(
            prev_month_rect.center(),
            Align2::CENTER_CENTER,
            "◀",
            egui::FontId::proportional(18.0),
            ui.visuals().text_color(),
        );
        if prev_month_response.clicked() {
            nav_result = (true, NavAction::PrevMonth);
            return nav_result;
        }

        // Draw month and year with clickable text that opens dropdowns
        let month_text_rect = Rect::from_min_size(
            title_rect.min,
            vec2(title_rect.width() / 2.0, title_rect.height()),
        );

        let year_text_rect = Rect::from_min_size(
            title_rect.min + vec2(title_rect.width() / 2.0, 0.0),
            vec2(title_rect.width() / 2.0, title_rect.height()),
        );

        // Month text and dropdown
        let month_text_response = ui.allocate_rect(month_text_rect, Sense::click());
        ui.painter().text(
            month_text_rect.center(),
            Align2::CENTER_CENTER,
            self.current_date.format("%B").to_string(),
            egui::FontId::proportional(18.0),
            ui.visuals().text_color(),
        );

        // Handle month scrolling when hovering
        if month_text_response.hovered() {
            self.handle_date_scroll(ui, true);
        }

        if month_text_response.clicked() {
            self.show_month_dropdown = !self.show_month_dropdown;
            // Close year dropdown if month dropdown is opened
            if self.show_month_dropdown {
                self.show_year_dropdown = false;
            }
        }

        // Handle month dropdown
        if self.show_month_dropdown {
            // Calculate dropdown width to match the text area width
            let dropdown_width = month_text_rect.width();

            Window::new("Month Selector")
                .fixed_pos(month_text_rect.left_bottom())
                .fixed_size([dropdown_width, 300.0])
                .title_bar(false)
                .show(ui.ctx(), |ui| {
                    ui.set_max_height(300.0);
                    egui::ScrollArea::vertical().show(ui, |ui| {
                        let months = [
                            "January", "February", "March", "April", "May", "June",
                            "July", "August", "September", "October", "November", "December"
                        ];

                        for (i, month_name) in months.iter().enumerate() {
                            let month_num = (i + 1) as u32;
                            let selected = month_num == self.current_date.month();

                            ui.horizontal(|ui| {
                                ui.with_layout(egui::Layout::centered_and_justified(egui::Direction::LeftToRight), |ui| {
                                    let month_response = ui.selectable_label(selected, *month_name);

                                    if month_response.clicked() {
                                        // Update the date with day preserved if possible
                                        let current_day = self.current_date.day();

                                        // Try to preserve the same day in the new month
                                        let new_date = match NaiveDate::from_ymd_opt(
                                            self.current_date.year(),
                                            month_num,
                                            current_day
                                        ) {
                                            Some(date) => date,
                                            None => {
                                                // If day is too high for the new month, use the last day of that month
                                                let last_day = if month_num == 12 {
                                                    NaiveDate::from_ymd_opt(self.current_date.year() + 1, 1, 1).unwrap() - Duration::days(1)
                                                } else {
                                                    NaiveDate::from_ymd_opt(self.current_date.year(), month_num + 1, 1).unwrap() - Duration::days(1)
                                                };
                                                last_day
                                            }
                                        };

                                        self.current_date = new_date;
                                        self.show_month_dropdown = false;
                                        // Reset clicked date when changing month
                                        self.last_clicked_date = None;
                                    }
                                });
                            });
                        }
                    });
                });
        }

        // Year text and dropdown
        let year_text_response = ui.allocate_rect(year_text_rect, Sense::click());
        ui.painter().text(
            year_text_rect.center(),
            Align2::CENTER_CENTER,
            self.current_date.format("%Y").to_string(),
            egui::FontId::proportional(18.0),
            ui.visuals().text_color(),
        );

        // Handle year scrolling when hovering
        if year_text_response.hovered() {
            self.handle_date_scroll(ui, false);
        }

        if year_text_response.clicked() {
            self.show_year_dropdown = !self.show_year_dropdown;
            // Close month dropdown if year dropdown is opened
            if self.show_year_dropdown {
                self.show_month_dropdown = false;
            }
        }

        // Handle year dropdown
        if self.show_year_dropdown {
            // Calculate dropdown width to match the text area width
            let dropdown_width = year_text_rect.width();

            Window::new("Year Selector")
                .fixed_pos(year_text_rect.left_bottom())
                .fixed_size([dropdown_width, 300.0])
                .title_bar(false)
                .show(ui.ctx(), |ui| {
                    ui.set_max_height(300.0);
                    egui::ScrollArea::vertical().show(ui, |ui| {
                        for year in self.min_year..=self.max_year {
                            let selected = year == self.current_date.year();

                            ui.horizontal(|ui| {
                                ui.with_layout(egui::Layout::centered_and_justified(egui::Direction::LeftToRight), |ui| {
                                    if ui.selectable_label(selected, year.to_string()).clicked() {
                                        // Handle leap year edge case
                                        if self.current_date.month() == 2 && self.current_date.day() == 29 && !is_leap_year(year) {
                                            self.current_date = NaiveDate::from_ymd_opt(
                                                year,
                                                2,
                                                28
                                            ).unwrap();
                                        } else {
                                            self.current_date = NaiveDate::from_ymd_opt(
                                                year,
                                                self.current_date.month(),
                                                self.current_date.day()
                                            ).unwrap_or_else(|| NaiveDate::from_ymd_opt(
                                                year,
                                                self.current_date.month(),
                                                1
                                            ).unwrap());
                                        }
                                        self.show_year_dropdown = false;
                                        // Reset clicked date when changing year
                                        self.last_clicked_date = None;
                                    }
                                });
                            });
                        }
                    });
                });
        }

        // Draw and handle next month button
        let next_month_response = ui.allocate_rect(next_month_rect, Sense::click());
        if next_month_response.hovered() {
            ui.painter().rect_filled(
                next_month_rect,
                0.0,
                Color32::from_white_alpha(10),
            );
        }
        ui.painter().text(
            next_month_rect.center(),
            Align2::CENTER_CENTER,
            "▶",
            egui::FontId::proportional(18.0),
            ui.visuals().text_color(),
        );
        if next_month_response.clicked() {
            nav_result = (true, NavAction::NextMonth);
            return nav_result;
        }

        // Draw and handle next year button
        let next_year_response = ui.allocate_rect(next_year_rect, Sense::click());
        if next_year_response.hovered() {
            ui.painter().rect_filled(
                next_year_rect,
                0.0,
                Color32::from_white_alpha(10),
            );
        }
        ui.painter().text(
            next_year_rect.center(),
            Align2::CENTER_CENTER,
            "▶▶",
            egui::FontId::proportional(18.0),
            ui.visuals().text_color(),
        );
        if next_year_response.clicked() {
            nav_result = (true, NavAction::NextYear);
            return nav_result;
        }

        nav_result
    }

    fn handle_date_scroll(&mut self, ui: &Ui, is_month: bool) {
        let scroll_delta = ui.input(|i| i.raw_scroll_delta.y);
        if scroll_delta == 0.0 {
            return;
        }

        // Use a reasonable threshold to avoid overly sensitive scrolling
        let threshold = 5.0;
        if scroll_delta.abs() < threshold {
            return;
        }

        if is_month {
            // Scroll for month navigation
            if scroll_delta > 0.0 {
                // Scroll up - go to previous month
                if self.current_date.month() == 1 {
                    // Go to December of previous year
                    self.current_date = NaiveDate::from_ymd_opt(
                        self.current_date.year() - 1,
                        12,
                        self.current_date.day().min(31)
                    ).unwrap();
                } else {
                    // Get the number of days in the previous month
                    let prev_month = self.current_date.month() - 1;
                    let prev_month_days = match prev_month {
                        2 => if is_leap_year(self.current_date.year()) { 29 } else { 28 },
                        4 | 6 | 9 | 11 => 30,
                        _ => 31,
                    };

                    // Ensure day is valid for the previous month
                    let day = self.current_date.day().min(prev_month_days);

                    self.current_date = NaiveDate::from_ymd_opt(
                        self.current_date.year(),
                        prev_month,
                        day
                    ).unwrap();
                }
            } else {
                // Scroll down - go to next month
                if self.current_date.month() == 12 {
                    // Go to January of next year
                    self.current_date = NaiveDate::from_ymd_opt(
                        self.current_date.year() + 1,
                        1,
                        self.current_date.day().min(31)
                    ).unwrap();
                } else {
                    // Get the number of days in the next month
                    let next_month = self.current_date.month() + 1;
                    let next_month_days = match next_month {
                        2 => if is_leap_year(self.current_date.year()) { 29 } else { 28 },
                        4 | 6 | 9 | 11 => 30,
                        _ => 31,
                    };

                    // Ensure day is valid for the next month
                    let day = self.current_date.day().min(next_month_days);

                    self.current_date = NaiveDate::from_ymd_opt(
                        self.current_date.year(),
                        next_month,
                        day
                    ).unwrap();
                }
            }
        } else {
            // Scroll for year navigation
            if scroll_delta > 0.0 {
                // Scroll up - go to previous year
                if self.current_date.month() == 2 && self.current_date.day() == 29 && !is_leap_year(self.current_date.year() - 1) {
                    self.current_date = NaiveDate::from_ymd_opt(
                        self.current_date.year() - 1,
                        2,
                        28
                    ).unwrap();
                } else {
                    self.current_date = NaiveDate::from_ymd_opt(
                        self.current_date.year() - 1,
                        self.current_date.month(),
                        self.current_date.day()
                    ).unwrap();
                }
            } else {
                // Scroll down - go to next year
                if self.current_date.month() == 2 && self.current_date.day() == 29 && !is_leap_year(self.current_date.year() + 1) {
                    self.current_date = NaiveDate::from_ymd_opt(
                        self.current_date.year() + 1,
                        2,
                        28
                    ).unwrap();
                } else {
                    self.current_date = NaiveDate::from_ymd_opt(
                        self.current_date.year() + 1,
                        self.current_date.month(),
                        self.current_date.day()
                    ).unwrap();
                }
            }
        }

        // Reset clicked date when changing date via scroll
        self.last_clicked_date = None;
    }

    // Update the draw_weekday_headers method to respect the week start day
    fn draw_weekday_headers(&self, ui: &mut Ui, rect: Rect) {
        let day_width = rect.width() / 7.0;

        // Create an array of weekday names in the proper order
        let all_weekdays = [
            "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"
        ];

        // Reorder the weekdays based on the start day
        let start_idx = self.week_start_day.num_days_from_monday() as usize;
        let mut weekdays = Vec::with_capacity(7);

        for i in 0..7 {
            let idx = (start_idx + i) % 7;
            weekdays.push(all_weekdays[idx]);
        }

        // Draw the weekday headers
        for (i, day) in weekdays.iter().enumerate() {
            let header_rect = Rect::from_min_size(
                rect.min + vec2(day_width * i as f32, 0.0),
                vec2(day_width, rect.height()),
            );

            ui.painter().text(
                header_rect.center(),
                Align2::CENTER_CENTER,
                *day,
                egui::FontId::proportional(14.0),
                ui.visuals().text_color(),
            );
        }
    }

    // Add a method to toggle cell expansion
    fn toggle_popout(&mut self, date: NaiveDate, cell_rect: Rect) {
        if self.popout_date == Some(date) {
            self.popout_date = None;
        } else {
            self.popout_date = Some(date);
            self.popout_cell_rect = Some(cell_rect);
        }
    }

    // Modified draw_calendar_grid to handle cell expansion
    fn draw_calendar_grid(&mut self, ui: &mut Ui, rect: Rect) -> Option<NaiveDate> {
        let (dates, num_weeks) = self.get_month_dates();
        let day_width = rect.width() / 7.0;
        let day_height = rect.height() / num_weeks as f32;
        let mut clicked_date = None;

        for (i, date_opt) in dates.iter().enumerate() {
            let row = i / 7;
            let col = i % 7;

            let cell_height = day_height;
            let day_rect = Rect::from_min_size(
                rect.min + vec2(day_width * col as f32, day_height * row as f32),
                vec2(day_width, cell_height),
            );

            // Draw day border and background
            ui.painter().rect_stroke(
                day_rect,
                0.0,
                Stroke::new(1.0, Color32::GRAY),
            );

            if let Some(date) = date_opt {
                // Get task count for this day
                let task_count = self.tasks_per_cell.get(date).map_or(0, |tasks| tasks.len());

                // Check if "more" indicator should be shown
                let show_more = task_count > self.max_visible_tasks;
                let more_count = if show_more { task_count - self.max_visible_tasks } else { 0 };

                // Create two separate interact areas: one for the indicator, one for the cell
                if show_more {
                    let indicator_height = 15.0;
                    let indicator_rect = Rect::from_min_size(
                        day_rect.left_bottom() - vec2(0.0, indicator_height),
                        vec2(day_rect.width(), indicator_height),
                    );

                    // The cell area excludes the indicator area
                    let cell_click_rect = Rect::from_min_size(
                        day_rect.min,
                        vec2(day_rect.width(), day_rect.height() - indicator_height),
                    );

                    // Handle cell click (excluding indicator area)
                    let cell_response = ui.allocate_rect(cell_click_rect, Sense::click());
                    if cell_response.clicked() {
                        clicked_date = Some(*date);
                    }

                    // Draw hover highlight for cell area
                    if cell_response.hovered() {
                        ui.painter().rect_filled(
                            cell_click_rect,
                            0.0,
                            Color32::from_white_alpha(10),
                        );
                    }

                    // Handle indicator separately
                    let indicator_clicked = self.draw_more_indicator(ui, day_rect, more_count);
                    if indicator_clicked {
                        self.toggle_popout(*date, day_rect);
                        // Don't set clicked_date - we're handling this separately
                    }
                } else {
                    // No indicator, handle whole cell click
                    let cell_response = ui.allocate_rect(day_rect, Sense::click());
                    if cell_response.clicked() {
                        clicked_date = Some(*date);
                    }

                    // Draw hover highlight
                    if cell_response.hovered() {
                        ui.painter().rect_filled(
                            day_rect,
                            0.0,
                            Color32::from_white_alpha(10),
                        );
                    }
                }

                // Draw day number
                let text_color = if *date == chrono::Local::now().date_naive() {
                    Color32::LIGHT_BLUE
                } else {
                    ui.visuals().text_color()
                };

                ui.painter().text(
                    day_rect.left_top() + vec2(day_width / 2.0, 5.0),
                    Align2::CENTER_TOP,
                    date.format("%d").to_string(),
                    egui::FontId::proportional(14.0),
                    text_color,
                );
            } else {
                // Handle dates outside current month
                let cell_response = ui.allocate_rect(day_rect, Sense::click());
                if cell_response.hovered() {
                    ui.painter().rect_filled(
                        day_rect,
                        0.0,
                        Color32::from_white_alpha(10),
                    );
                }
            }
        }

        clicked_date
    }

    // Draw a "N more" indicator at the bottom of a cell and return true if it was clicked
    fn draw_more_indicator(&self, ui: &mut Ui, cell_rect: Rect, more_count: usize) -> bool {
        // Create a larger bar at the bottom of the cell
        let indicator_height = 20.0; // Increased from 15.0
        let indicator_rect = Rect::from_min_size(
            cell_rect.left_bottom() - vec2(0.0, indicator_height),
            vec2(cell_rect.width(), indicator_height),
        );

        // Allocate click response for the indicator
        let indicator_response = ui.allocate_rect(indicator_rect, Sense::click());

        // Draw the indicator background with a slightly rounded appearance
        let bg_color = if indicator_response.hovered() {
            Color32::from_gray(100) // Lighter when hovered
        } else {
            Color32::from_gray(70)
        };

        ui.painter().rect_filled(
            indicator_rect,
            2.0, // Add slight rounding
            bg_color,
        );

        // Draw the text
        let text = format!("+ {} more", more_count);
        ui.painter().text(
            indicator_rect.center(),
            Align2::CENTER_CENTER,
            text,
            egui::FontId::proportional(11.0), // Slightly larger font
            Color32::WHITE,
        );

        indicator_response.clicked()
    }

    fn draw_tasks(&mut self, ui: &mut Ui, tasks: &[TaskViewMonth], rect: Rect) -> Option<u32> {
        let (dates, num_weeks) = self.get_month_dates();
        let day_width = rect.width() / 7.0;
        let day_height = rect.height() / num_weeks as f32;
        let mut clicked_task_id = None;

        // First, organize tasks by date and determine vertical positions
        self.tasks_per_cell.clear();

        // Map of positions already taken for each day
        let mut position_map: HashMap<NaiveDate, Vec<u32>> = HashMap::new();

        // Step 1: First pass to count tasks per cell and populate tasks_per_cell
        for task in tasks {
            let mut current_date = task.start_date;
            while current_date <= task.end_date {
                // Only include dates that belong to the current month view
                if current_date.year() == self.current_date.year() &&
                    current_date.month() == self.current_date.month() {
                    self.tasks_per_cell
                        .entry(current_date)
                        .or_insert_with(Vec::new)
                        .push(task.id);
                }
                current_date = current_date.succ_opt().unwrap_or(current_date);
            }
        }

        // Step 2: Assign vertical positions to tasks
        let mut task_positions: HashMap<u32, usize> = HashMap::new();

        for task in tasks {
            // Calculate the position for this task
            let mut best_position = 0;
            let mut position_taken = true;

            // Find first available position
            while position_taken {
                position_taken = false;

                // Check if this position is available for the entire span of the task
                let mut current_date = task.start_date;
                while !position_taken && current_date <= task.end_date {
                    if current_date.year() == self.current_date.year() &&
                        current_date.month() == self.current_date.month() {
                        // Check if this position is already taken on this date
                        if position_map
                            .get(&current_date)
                            .map_or(false, |positions| positions.contains(&(best_position as u32))) {
                            position_taken = true;
                        }
                    }
                    current_date = current_date.succ_opt().unwrap_or(current_date);
                }

                if position_taken {
                    best_position += 1;
                }
            }

            // Assign this position to the task
            task_positions.insert(task.id, best_position);

            // Mark this position as taken for all dates this task spans
            let mut current_date = task.start_date;
            while current_date <= task.end_date {
                if current_date.year() == self.current_date.year() &&
                    current_date.month() == self.current_date.month() {
                    position_map
                        .entry(current_date)
                        .or_insert_with(Vec::new)
                        .push(best_position as u32);
                }
                current_date = current_date.succ_opt().unwrap_or(current_date);
            }
        }

        // Now draw continuous task strips
        for task in tasks {
            // Only draw tasks that are in the current month view
            if (task.start_date.year() == self.current_date.year() &&
                task.start_date.month() == self.current_date.month()) ||
                (task.end_date.year() == self.current_date.year() &&
                    task.end_date.month() == self.current_date.month()) {

                // Find dates in our month view that this task spans
                let month_start = NaiveDate::from_ymd_opt(
                    self.current_date.year(),
                    self.current_date.month(),
                    1
                ).unwrap();

                let month_end = if self.current_date.month() == 12 {
                    NaiveDate::from_ymd_opt(
                        self.current_date.year() + 1,
                        1,
                        1
                    ).unwrap() - Duration::days(1)
                } else {
                    NaiveDate::from_ymd_opt(
                        self.current_date.year(),
                        self.current_date.month() + 1,
                        1
                    ).unwrap() - Duration::days(1)
                };

                // Calculate task span within current month view
                let task_start_in_view = if task.start_date < month_start {
                    month_start
                } else {
                    task.start_date
                };

                let task_end_in_view = if task.end_date > month_end {
                    month_end
                } else {
                    task.end_date
                };

                // Get task's vertical position
                let position = task_positions.get(&task.id).copied().unwrap_or(0);

                // Get date indices for start and end
                let mut start_idx = None;
                let mut end_idx = None;

                for (i, date_opt) in dates.iter().enumerate() {
                    if let Some(date) = date_opt {
                        if *date == task_start_in_view {
                            start_idx = Some(i);
                        }
                        if *date == task_end_in_view {
                            end_idx = Some(i);
                        }
                    }
                }

                // If we found valid start and end positions, draw the task
                if let (Some(start_idx), Some(end_idx)) = (start_idx, end_idx) {
                    let start_row = start_idx / 7;
                    let start_col = start_idx % 7;
                    let end_row = end_idx / 7;
                    let end_col = end_idx % 7;

                    // Check if this task should be visible based on expanded state
                    let visible = position < self.max_visible_tasks;

                    if visible {
                        // Draw task segments for each week it spans
                        if start_row == end_row {
                            // Task is contained within a single row
                            if let Some(clicked_id) = self.draw_task_segment(
                                ui,
                                task,
                                rect,
                                start_row,
                                start_col,
                                end_col - start_col + 1,
                                position,
                                task.start_date < month_start,
                                task.end_date > month_end,
                                day_width,
                                day_height,
                            ) {
                                clicked_task_id = Some(clicked_id);
                            }
                        } else {
                            // Task spans multiple rows

                            // First row (partial)
                            if let Some(clicked_id) = self.draw_task_segment(
                                ui,
                                task,
                                rect,
                                start_row,
                                start_col,
                                7 - start_col,
                                position,
                                task.start_date < month_start,
                                true, // Continues to next row
                                day_width,
                                day_height,
                            ) {
                                clicked_task_id = Some(clicked_id);
                            }

                            // Middle rows (if any)
                            for row in (start_row + 1)..end_row {
                                if let Some(clicked_id) = self.draw_task_segment(
                                    ui,
                                    task,
                                    rect,
                                    row,
                                    0,
                                    7,
                                    position,
                                    true, // Continues from previous row
                                    true, // Continues to next row
                                    day_width,
                                    day_height,
                                ) {
                                    clicked_task_id = Some(clicked_id);
                                }
                            }

                            // Last row (partial)
                            if let Some(clicked_id) = self.draw_task_segment(
                                ui,
                                task,
                                rect,
                                end_row,
                                0,
                                end_col + 1,
                                position,
                                true, // Continues from previous row
                                task.end_date > month_end,
                                day_width,
                                day_height,
                            ) {
                                clicked_task_id = Some(clicked_id);
                            }
                        }
                    }
                }
            }
        }

        clicked_task_id
    }

    // New method to draw a single task within a cell
    fn draw_task_in_cell(
        &self,
        ui: &mut Ui,
        task: &TaskViewMonth,
        rect: Rect,
        row: usize,
        col: usize,
        vertical_offset: f32,
        day_width: f32,
        day_height: f32
    ) -> Option<u32> {
        // Calculate task rectangle
        let task_rect = Rect::from_min_size(
            rect.min + vec2(
                day_width * col as f32 + 2.0, // Add small margin
                day_height * row as f32 + vertical_offset
            ),
            vec2(
                day_width - 4.0, // Subtract margin on both sides
                self.task_height - 2.0
            ),
        );

        // Allocate a response for the task rectangle
        let task_response = ui.allocate_rect(task_rect, Sense::click() | Sense::hover());

        // Draw the task background with hover effect
        let task_color = if task_response.hovered() {
            lighten_color(task.color, 0.2)
        } else {
            task.color
        };

        ui.painter().rect_filled(
            task_rect,
            4.0,
            task_color,
        );

        // Add a border
        ui.painter().rect_stroke(
            task_rect,
            4.0,
            Stroke::new(1.0, Color32::from_white_alpha(80)),
        );

        // Draw task title, truncated if needed
        let available_width = task_rect.width() - 10.0; // Account for padding
        let text = truncate_text(&task.title, available_width, ui);

        ui.painter().text(
            task_rect.left_center() + vec2(5.0, 0.0),
            Align2::LEFT_CENTER,
            text,
            egui::FontId::proportional(12.0),
            Color32::WHITE,
        );

        // Check if the task was clicked
        if task_response.clicked() {
            return Some(task.id);
        }

        None

    }

    fn draw_task_segment(
        &self,
        ui: &mut Ui,
        task: &TaskViewMonth,
        rect: Rect,
        row: usize,
        start_col: usize,
        span: usize,
        position: usize,
        continues_from_left: bool,
        continues_to_right: bool,
        day_width: f32,
        day_height: f32
    ) -> Option<u32> {
        // Calculate the vertical position of this task
        let vertical_offset = 20.0 + (position as f32 * self.task_height);

        // Calculate the task rectangle
        let task_rect = Rect::from_min_size(
            rect.min + vec2(
                day_width * start_col as f32 + (if continues_from_left { 0.0 } else { 2.0 }),
                day_height * row as f32 + vertical_offset
            ),
            vec2(
                day_width * span as f32 - (if continues_from_left { 0.0 } else { 2.0 }) - (if continues_to_right { 0.0 } else { 2.0 }),
                self.task_height - 2.0
            ),
        );

        // Allocate a response for the task rectangle
        let task_response = ui.allocate_rect(task_rect, Sense::click() | Sense::hover());

        // Draw the task background with hover effect
        let task_color = if task_response.hovered() {
            lighten_color(task.color, 0.2)
        } else {
            task.color
        };

        // Draw task with rounded corners on the appropriate sides
        let (left_radius, right_radius) = match (continues_from_left, continues_to_right) {
            (false, false) => (4.0, 4.0),      // Rounded on both sides
            (true, false) => (0.0, 4.0),       // Flat on left, rounded on right
            (false, true) => (4.0, 0.0),       // Rounded on left, flat on right
            (true, true) => (0.0, 0.0),        // Flat on both sides
        };

        if left_radius == right_radius {
            // If both sides have the same radius, draw a regular rounded rect
            ui.painter().rect_filled(
                task_rect,
                left_radius,
                task_color,
            );
        } else {
            // Draw a rect with different radii on each side
            // This requires custom path drawing
            let points = if left_radius > 0.0 && right_radius == 0.0 {
                // Left side rounded, right side flat
                vec![
                    task_rect.left_top() + vec2(left_radius, 0.0),
                    task_rect.right_top(),
                    task_rect.right_bottom(),
                    task_rect.left_bottom() + vec2(left_radius, 0.0),
                    task_rect.left_bottom() + vec2(0.0, -left_radius),
                    task_rect.left_top() + vec2(0.0, left_radius),
                ]
            } else if left_radius == 0.0 && right_radius > 0.0 {
                // Left side flat, right side rounded
                vec![
                    task_rect.left_top(),
                    task_rect.right_top() - vec2(right_radius, 0.0),
                    task_rect.right_top() + vec2(0.0, right_radius),
                    task_rect.right_bottom() + vec2(0.0, -right_radius),
                    task_rect.right_bottom() - vec2(right_radius, 0.0),
                    task_rect.left_bottom(),
                ]
            } else {
                // Fallback - just draw a rectangle
                vec![
                    task_rect.left_top(),
                    task_rect.right_top(),
                    task_rect.right_bottom(),
                    task_rect.left_bottom(),
                ]
            };

            ui.painter().add(Shape::convex_polygon(
                points,
                task_color,
                Stroke::NONE,
            ));
        }

        // Add arrow indicators for continuation
        if continues_to_right {
            // Draw a right-pointing arrow
            let arrow_center = task_rect.right_center() - vec2(8.0, 0.0);
            let arrow_points = vec![
                arrow_center + vec2(4.0, 0.0),
                arrow_center + vec2(-2.0, -4.0),
                arrow_center + vec2(-2.0, 4.0),
            ];

            ui.painter().add(Shape::convex_polygon(
                arrow_points,
                Color32::WHITE,
                Stroke::NONE,
            ));
        }

        if continues_from_left {
            // Draw a left-pointing arrow
            let arrow_center = task_rect.left_center() + vec2(8.0, 0.0);
            let arrow_points = vec![
                arrow_center + vec2(-4.0, 0.0),
                arrow_center + vec2(2.0, -4.0),
                arrow_center + vec2(2.0, 4.0),
            ];

            ui.painter().add(Shape::convex_polygon(
                arrow_points,
                Color32::WHITE,
                Stroke::NONE,
            ));
        }

        // Draw task title if there's enough space
        if span > 1 {
            // Always draw text, but possibly truncate for very small spans
            let available_width = task_rect.width() - 10.0
                - (if continues_from_left { 8.0 } else { 0.0 })
                - (if continues_to_right { 8.0 } else { 0.0 });

            // For narrow cells, still attempt to show something
            let text = if span <= 1 && available_width < 30.0 {
                "•".to_string() // Show a bullet for very small cells
            } else {
                truncate_text(&task.title, available_width, ui)
            };

            let text_pos = task_rect.left_center() + vec2(
                5.0 + (if continues_from_left { 8.0 } else { 0.0 }),
                0.0
            );

            ui.painter().text(
                text_pos,
                Align2::LEFT_CENTER,
                text,
                egui::FontId::proportional(12.0),
                Color32::WHITE,
            );
        }

        // Check if the task was clicked
        if task_response.clicked() {
            return Some(task.id);
        }

        None
    }

    // Draw popout window for a date with all tasks
    fn draw_popout_window(&self, ui: &mut Ui, date: NaiveDate, tasks: &[TaskViewMonth], cell_rect: Rect) {
        // Get day name and format date
        let day_name = date.format("%a").to_string().to_uppercase();
        let day_num = date.format("%d").to_string();

        // Create window title
        let title = format!("{} {}", day_name, day_num);

        // Filter tasks for this date
        let date_tasks: Vec<&TaskViewMonth> = tasks.iter()
            .filter(|task| task.start_date <= date && task.end_date >= date)
            .collect();

        // Calculate estimated window dimensions
        let task_height = 34.0; // 30px height + 4px spacing
        let estimated_content_height = date_tasks.len() as f32 * task_height;
        let max_window_height = 400.0;
        let window_height = estimated_content_height.min(max_window_height) + 40.0; // Add header height
        let window_width = 300.0;

        // Calculate window position, accounting for screen edges
        let screen_size = ui.ctx().screen_rect();
        let mut window_pos = cell_rect.left_top() + vec2(10.0, 10.0);

        // Check if window would go below screen bottom
        if window_pos.y + window_height > screen_size.max.y {
            // Position window above the cell instead
            window_pos.y = cell_rect.max.y - window_height - 50.0;
        }

        // Check if window would go beyond right edge
        if window_pos.x + window_width > screen_size.max.x {
            window_pos.x = cell_rect.max.x - window_width - 10.0;
        }

        // Show the window
        Window::new(&title)
            .fixed_size([window_width, window_height])
            .collapsible(false)
            .resizable(false)
            .fixed_pos(window_pos)
            .show(ui.ctx(), |ui| {
                // Draw tasks in a scrollable area
                egui::ScrollArea::vertical().show(ui, |ui| {
                    for task in date_tasks {
                        ui.add_space(4.0);

                        // Create rect for this task
                        let available_width = ui.available_width();
                        let task_rect_height = 30.0;

                        // Draw background for the task
                        let (task_rect, response) = ui.allocate_at_least(
                            vec2(available_width, task_rect_height),
                            Sense::click()
                        );

                        ui.painter().rect_filled(task_rect, 4.0, task.color);

                        // Draw task title
                        ui.painter().text(
                            task_rect.left_center() + vec2(8.0, 0.0),
                            Align2::LEFT_CENTER,
                            &task.title,
                            egui::FontId::proportional(14.0),
                            Color32::WHITE,
                        );
                    }
                });
            });
    }

    pub fn show(&mut self, ui: &mut Ui, tasks: &[TaskViewMonth]) -> Response {
        // Reset so that values are only Some when they were clicked in this frame
        self.last_clicked_date = None;
        self.last_clicked_task = None;

        // Close dropdowns on click outside
        if ui.input(|i| i.pointer.any_pressed()) {
            if self.show_month_dropdown || self.show_year_dropdown {
                let pointer_pos = ui.input(|i| i.pointer.interact_pos());
                if let Some(pos) = pointer_pos {
                    let month_header_height = 40.0;
                    let month_header_rect = Rect::from_min_size(
                        ui.next_widget_position(),
                        vec2(self.width, month_header_height),
                    );

                    // Calculate the approximate dropdown area
                    let section_width = self.width / 5.0;
                    let dropdown_width = month_header_rect.width() / 5.0;
                    let dropdown_height = 300.0;

                    // Create a rect for the month dropdown area
                    let month_dropdown_rect = if self.show_month_dropdown {
                        let dropdown_x = month_header_rect.min.x + section_width * 2.0;
                        Rect::from_min_size(
                            egui::pos2(dropdown_x, month_header_rect.max.y),
                            vec2(dropdown_width, dropdown_height),
                        )
                    } else {
                        Rect::NOTHING
                    };

                    // Create a rect for the year dropdown area
                    let year_dropdown_rect = if self.show_year_dropdown {
                        let dropdown_x = month_header_rect.min.x + section_width * 2.5;
                        Rect::from_min_size(
                            egui::pos2(dropdown_x, month_header_rect.max.y),
                            vec2(dropdown_width, dropdown_height),
                        )
                    } else {
                        Rect::NOTHING
                    };

                    // Only close if clicking outside the header AND outside both dropdowns
                    if !month_header_rect.contains(pos) &&
                        !month_dropdown_rect.contains(pos) &&
                        !year_dropdown_rect.contains(pos) {
                        self.show_month_dropdown = false;
                        self.show_year_dropdown = false;
                    }
                }
            }
        }

        let (rect, response) = ui.allocate_exact_size(
            vec2(self.width, self.height),
            Sense::click_and_drag(),
        );

        // Draw background
        ui.painter().rect_filled(
            rect,
            0.0,
            ui.visuals().extreme_bg_color,
        );

        // Create layout rects
        let month_header_height = 40.0;
        let month_header_rect = Rect::from_min_size(
            rect.min,
            vec2(rect.width(), month_header_height),
        );

        let weekday_header_rect = Rect::from_min_size(
            rect.min + vec2(0.0, month_header_height),
            vec2(rect.width(), self.day_header_height),
        );

        let calendar_rect = Rect::from_min_size(
            rect.min + vec2(0.0, month_header_height + self.day_header_height),
            vec2(rect.width(), rect.height() - month_header_height - self.day_header_height),
        );

        // Draw components and handle navigation
        let (nav_occurred, nav_action) = self.draw_month_header(ui, month_header_rect);

        // Handle month/year navigation from buttons
        if nav_occurred {
            self.popout_date = None;
            match nav_action {
                // ... navigation handling (code unchanged)
                NavAction::PrevMonth => {
                    // Go to previous month
                    if self.current_date.month() == 1 {
                        // Go to December of previous year
                        self.current_date = NaiveDate::from_ymd_opt(
                            self.current_date.year() - 1,
                            12,
                            self.current_date.day().min(31)
                        ).unwrap();
                    } else {
                        // Get the number of days in the previous month
                        let prev_month = self.current_date.month() - 1;
                        let prev_month_days = match prev_month {
                            2 => if is_leap_year(self.current_date.year()) { 29 } else { 28 },
                            4 | 6 | 9 | 11 => 30,
                            _ => 31,
                        };

                        // Ensure day is valid for the previous month
                        let day = self.current_date.day().min(prev_month_days);

                        self.current_date = NaiveDate::from_ymd_opt(
                            self.current_date.year(),
                            prev_month,
                            day
                        ).unwrap();
                    }
                    self.last_clicked_date = None;
                },
                NavAction::NextMonth => {
                    // Go to next month
                    if self.current_date.month() == 12 {
                        // Go to January of next year
                        self.current_date = NaiveDate::from_ymd_opt(
                            self.current_date.year() + 1,
                            1,
                            self.current_date.day().min(31)
                        ).unwrap();
                    } else {
                        // Get the number of days in the next month
                        let next_month = self.current_date.month() + 1;
                        let next_month_days = match next_month {
                            2 => if is_leap_year(self.current_date.year()) { 29 } else { 28 },
                            4 | 6 | 9 | 11 => 30,
                            _ => 31,
                        };

                        // Ensure day is valid for the next month
                        let day = self.current_date.day().min(next_month_days);

                        self.current_date = NaiveDate::from_ymd_opt(
                            self.current_date.year(),
                            next_month,
                            day
                        ).unwrap();
                    }
                    self.last_clicked_date = None;
                },
                NavAction::PrevYear => {
                    // Go to previous year (same month)
                    if self.current_date.month() == 2 && self.current_date.day() == 29 {
                        // Handle leap year edge case
                        self.current_date = NaiveDate::from_ymd_opt(
                            self.current_date.year() - 1,
                            2,
                            28
                        ).unwrap();
                    } else {
                        // Normal case
                        self.current_date = NaiveDate::from_ymd_opt(
                            self.current_date.year() - 1,
                            self.current_date.month(),
                            self.current_date.day()
                        ).unwrap();
                    }
                    self.last_clicked_date = None;
                },
                NavAction::NextYear => {
                    // Go to next year (same month)
                    if self.current_date.month() == 2 && self.current_date.day() == 29 {
                        // Handle leap year edge case
                        self.current_date = NaiveDate::from_ymd_opt(
                            self.current_date.year() + 1,
                            2,
                            28
                        ).unwrap();
                    } else {
                        // Normal case
                        self.current_date = NaiveDate::from_ymd_opt(
                            self.current_date.year() + 1,
                            self.current_date.month(),
                            self.current_date.day()
                        ).unwrap();
                    }
                    self.last_clicked_date = None;
                },
                NavAction::None => {}
            }
        }

        self.draw_weekday_headers(ui, weekday_header_rect);

        // Handle date selection from calendar grid
        if let Some(clicked) = self.draw_calendar_grid(ui, calendar_rect) {
            self.last_clicked_date = Some(clicked);
        } else if response.clicked() && !self.show_month_dropdown && !self.show_year_dropdown {
            self.last_clicked_date = None;
        }

        // Draw tasks and check for task clicks
        if let Some(task_id) = self.draw_tasks(ui, tasks, calendar_rect) {
            self.last_clicked_task = Some(task_id);
        }

        // Handle keyboard navigation (code unchanged)
        if ui.input(|i| i.key_pressed(egui::Key::ArrowLeft)) {
            // Previous month
            let prev_month = if self.current_date.month() == 1 {
                (self.current_date.year() - 1, 12)
            } else {
                (self.current_date.year(), self.current_date.month() - 1)
            };

            // Get the number of days in the previous month
            let prev_month_days = match prev_month.1 {
                2 => if is_leap_year(prev_month.0) { 29 } else { 28 },
                4 | 6 | 9 | 11 => 30,
                _ => 31,
            };

            // Ensure day is valid for the previous month
            let day = self.current_date.day().min(prev_month_days);

            self.current_date = NaiveDate::from_ymd_opt(
                prev_month.0,
                prev_month.1,
                day
            ).unwrap();
            self.last_clicked_date = None;
        }

        if ui.input(|i| i.key_pressed(egui::Key::ArrowRight)) {
            // Next month
            let next_month = if self.current_date.month() == 12 {
                (self.current_date.year() + 1, 1)
            } else {
                (self.current_date.year(), self.current_date.month() + 1)
            };

            // Get the number of days in the next month
            let next_month_days = match next_month.1 {
                2 => if is_leap_year(next_month.0) { 29 } else { 28 },
                4 | 6 | 9 | 11 => 30,
                _ => 31,
            };

            // Ensure day is valid for the next month
            let day = self.current_date.day().min(next_month_days);

            self.current_date = NaiveDate::from_ymd_opt(
                next_month.0,
                next_month.1,
                day
            ).unwrap();
            self.last_clicked_date = None;
        }

        if ui.input(|i| i.key_pressed(egui::Key::ArrowUp)) {
            // Previous year
            if self.current_date.month() == 2 && self.current_date.day() == 29 && !is_leap_year(self.current_date.year() - 1) {
                self.current_date = NaiveDate::from_ymd_opt(
                    self.current_date.year() - 1,
                    2,
                    28
                ).unwrap();
            } else {
                self.current_date = NaiveDate::from_ymd_opt(
                    self.current_date.year() - 1,
                    self.current_date.month(),
                    self.current_date.day()
                ).unwrap();
            }
            self.last_clicked_date = None;
        }

        if ui.input(|i| i.key_pressed(egui::Key::ArrowDown)) {
            // Next year
            if self.current_date.month() == 2 && self.current_date.day() == 29 && !is_leap_year(self.current_date.year() + 1) {
                self.current_date = NaiveDate::from_ymd_opt(
                    self.current_date.year() + 1,
                    2,
                    28
                ).unwrap();
            } else {
                self.current_date = NaiveDate::from_ymd_opt(
                    self.current_date.year() + 1,
                    self.current_date.month(),
                    self.current_date.day()
                ).unwrap();
            }
            self.last_clicked_date = None;
        }

        if let Some(date) = self.popout_date && let Some(cell_rect) = self.popout_cell_rect {
            self.draw_popout_window(ui, date, tasks, cell_rect);
        }

        response
    }
}

// Helper function to check if a year is a leap year
fn is_leap_year(year: i32) -> bool {
    (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)
}

// Helper function to lighten a color for hover effects
fn lighten_color(color: Color32, amount: f32) -> Color32 {
    let [r, g, b, a] = color.to_array();
    let r = ((r as f32) + (amount * 255.0)).min(255.0) as u8;
    let g = ((g as f32) + (amount * 255.0)).min(255.0) as u8;
    let b = ((b as f32) + (amount * 255.0)).min(255.0) as u8;
    Color32::from_rgba_unmultiplied(r, g, b, a)
}

fn truncate_text(text: &str, max_width: f32, ui: &Ui) -> String {
    let font_id = egui::FontId::proportional(12.0);
    let text_galley = ui.painter().layout(
        text.to_string(),
        font_id.clone(),
        Color32::WHITE,
        max_width,
    );

    if text_galley.rect.width() <= max_width {
        return text.to_string();
    }

    // Simple fallback strategy - just use a fixed number of characters based on width
    let chars_that_fit = ((text.len() as f32) * (max_width / text_galley.rect.width())).floor() as usize;
    if chars_that_fit > 3 {
        // Leave room for ellipsis
        let truncated = format!("{}...", &text[0..chars_that_fit.saturating_sub(3)]);
        return truncated;
    }

    "...".to_string()
}
