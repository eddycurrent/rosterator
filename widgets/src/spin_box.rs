use egui::{vec2, Color32, Frame, Response, Sense, Shape, Stroke, TextEdit, Ui, Vec2};
use std::ops::{Add, AddAssign, Sub, SubAssign};
use std::str::FromStr;
use egui::epaint::PathShape;

pub struct SpinBox {
    width: f32,
    button_color: Color32,
    text_color: Option<Color32>,
    button_size: Vec2,
}

impl Default for SpinBox {
    fn default() -> Self {
        Self {
            width: 30.0,
            button_color: Color32::GRAY,
            text_color: None,
            button_size: vec2(10.0, 8.0),
        }
    }
}

impl SpinBox {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn width(mut self, width: f32) -> Self {
        self.width = width;
        self
    }

    pub fn button_color(mut self, color: Color32) -> Self {
        self.button_color = color;
        self
    }

    pub fn text_color(mut self, color: Color32) -> Self {
        self.text_color = Some(color);
        self
    }

    pub fn button_size(mut self, size: Vec2) -> Self {
        self.button_size = size;
        self
    }

    fn draw_up_button(&self, ui: &mut Ui) -> Response {
        let frame_response = Frame::none().show(ui, |ui| {
            let (response, painter) = ui.allocate_painter(self.button_size, Sense::click());
            let origin = response.rect.min;
            painter.add(Shape::Path(PathShape::convex_polygon(
                vec![
                    origin + vec2(0.0, self.button_size.y),
                    origin + vec2(self.button_size.x / 2.0, 0.0),
                    origin + vec2(self.button_size.x, self.button_size.y),
                    origin + vec2(0.0, self.button_size.y),
                ],
                self.button_color,
                Stroke::NONE,
            )));
            response
        });
        frame_response.response.interact(Sense::click())
    }

    fn draw_down_button(&self, ui: &mut Ui) -> Response {
        let frame_response = Frame::none().show(ui, |ui| {
            let (response, painter) = ui.allocate_painter(self.button_size, Sense::click());
            let origin = response.rect.min;
            painter.add(Shape::Path(PathShape::convex_polygon(
                vec![
                    origin + vec2(0.0, 0.0),
                    origin + vec2(self.button_size.x / 2.0, self.button_size.y),
                    origin + vec2(self.button_size.x, 0.0),
                    origin + vec2(0.0, 0.0),
                ],
                self.button_color,
                Stroke::NONE,
            )));
            response
        });
        frame_response.response.interact(Sense::click())
    }

    fn handle_scroll<T>(
        &self,
        ui: &mut Ui,
        value: &mut T,
        lower_limit: &T,
        upper_limit: &T,
        increment: &T,
    ) where
        T: Add<Output = T> + AddAssign + Sub<Output = T> + SubAssign + Clone + PartialOrd,
    {
        let mut stored_delta = ui
            .ctx()
            .data_mut(|data| data.get_persisted("spin box".into()).unwrap_or(0f32));

        for event in ui.ctx().input(|input| input.events.clone()) {
            if let egui::Event::MouseWheel { delta, .. } = event {
                stored_delta += delta.y;
                if stored_delta > 0.999 && value <= &mut (upper_limit.clone() - increment.clone()) {
                    *value += increment.clone();
                    stored_delta = 0.0;
                } else if stored_delta <= -0.999 && value >= &mut (lower_limit.clone() + increment.clone())
                {
                    *value -= increment.clone();
                    stored_delta = 0.0;
                } else if stored_delta > 1.001 || stored_delta < -1.001 {
                    stored_delta = 0.0;
                }
            }
        }

        ui.ctx()
            .data_mut(|data| data.insert_persisted("spin box".into(), stored_delta));
    }

    pub fn show<T>(
        &mut self,
        ui: &mut Ui,
        value: &mut T,
        lower_limit: T,
        upper_limit: T,
        increment: T,
    ) -> bool
    where
        T: Add<Output = T>
        + AddAssign
        + Sub<Output = T>
        + SubAssign
        + ToString
        + PartialOrd
        + FromStr
        + Clone,
    {
        let start_value = value.clone();

        ui.horizontal(|ui| {
            ui.spacing_mut().item_spacing = vec2(0.0, 1.0);

            // Text input with UI-based text color
            let mut text = value.to_string();
            let text_edit = TextEdit::singleline(&mut text)
                .desired_width(self.width)
                .text_color(self.text_color.unwrap_or(ui.visuals().text_color()));

            let text_edit_response = ui.add(text_edit);

            if let Ok(number) = text.parse() {
                *value = number;
            }

            // Handle scrolling when hovering over text input
            if text_edit_response.contains_pointer() {
                self.handle_scroll(ui, value, &lower_limit, &upper_limit, &increment);
            }

            // Buttons
            ui.vertical(|ui| {
                let up_response = self.draw_up_button(ui);
                if up_response.clicked() && *value <= upper_limit.clone() - increment.clone() {
                    *value += increment.clone();
                }

                let down_response = self.draw_down_button(ui);
                if down_response.clicked() && *value >= lower_limit.clone() + increment.clone() {
                    *value -= increment;
                }
            });
        });

        start_value != *value
    }
}