use crate::SpinBox;
use chrono::{Datelike, Duration, NaiveDate};
use egui::{vec2, Align2, Color32, Frame, Response, Sense, Stroke, Ui};
use std::string::ToString;

pub struct DatePicker {
    width: f32,
    height: f32,
    selected_color: Color32,
    hover_color: Color32,
    today_color: Color32,
}

impl Default for DatePicker {
    fn default() -> Self {
        Self {
            width: 280.0,
            height: 320.0,
            selected_color: Color32::from_rgb(100, 150, 255),
            hover_color: Color32::from_rgba_premultiplied(100, 150, 255, 100),
            today_color: Color32::from_rgb(150, 200, 255),
        }
    }
}

impl DatePicker {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn width(mut self, width: f32) -> Self {
        self.width = width;
        self
    }

    pub fn height(mut self, height: f32) -> Self {
        self.height = height;
        self
    }

    pub fn selected_color(mut self, color: Color32) -> Self {
        self.selected_color = color;
        self
    }

    pub fn hover_color(mut self, color: Color32) -> Self {
        self.hover_color = color;
        self
    }

    pub fn today_color(mut self, color: Color32) -> Self {
        self.today_color = color;
        self
    }

    fn draw_calendar_header(&self, ui: &mut Ui, date: &mut NaiveDate) -> bool {
        let mut changed = false;

        ui.vertical_centered(|ui| {
            ui.horizontal(|ui| {
                ui.spacing_mut().item_spacing.x = 0.0;

                // Month and year with fixed width
                let label_width = 120.0;
                // Left arrow with fixed width, should fill the remaining space either side of the
                // label
                let arrow_width = (ui.available_width() - label_width) / 2.0;

                let (rect, response) = ui.allocate_exact_size(
                    vec2(arrow_width, 20.0),
                    Sense::click());
                if response.clicked() {
                    *date = date.checked_sub_months(chrono::Months::new(1)).unwrap_or(*date);
                    changed = true;
                }

                if response.hovered() {
                    ui.painter().rect_filled(rect, 3.0, self.hover_color);
                }

                ui.painter().text(
                    rect.center(),
                    Align2::CENTER_CENTER,
                    "◀",
                    egui::FontId::proportional(14.0),
                    ui.visuals().text_color()
                );

                let (rect, _) = ui.allocate_exact_size(vec2(label_width, 20.0), Sense::hover());
                ui.painter().text(
                    rect.center(),
                    Align2::CENTER_CENTER,
                    format!("{} {}", date.format("%B"), date.year()),
                    egui::FontId::proportional(14.0),
                    ui.visuals().text_color()
                );

                // Right arrow with fixed width
                let (rect, response) = ui.allocate_exact_size(
                    vec2(arrow_width, 20.0),
                    Sense::click());
                if response.clicked() {
                    *date = date.checked_add_months(chrono::Months::new(1)).unwrap_or(*date);
                    changed = true;
                }

                if response.hovered() {
                    ui.painter().rect_filled(rect, 3.0, self.hover_color);
                }

                ui.painter().text(
                    rect.center(),
                    Align2::CENTER_CENTER,
                    "▶",
                    egui::FontId::proportional(14.0),
                    ui.visuals().text_color()
                );
            });
        });

        changed
    }

    fn draw_weekday_headers(&self, ui: &mut Ui) {
        Frame::none()
            .show(ui, |ui| {
                ui.horizontal(|ui| {
                    for weekday in &["Su", "Mo", "Tu", "We", "Th", "Fr", "Sa"] {
                        let (rect, _) = ui.allocate_exact_size(
                            vec2(30.0, 20.0),
                            Sense::hover()
                        );
                        ui.painter().text(
                            rect.center(),
                            Align2::CENTER_CENTER,
                            weekday,
                            egui::FontId::proportional(14.0),
                            ui.visuals().text_color()
                        );
                    }
                });
            });
    }

    fn get_month_grid(&self, date: NaiveDate) -> Vec<Option<NaiveDate>> {
        let mut grid = Vec::with_capacity(42); // 6 rows * 7 columns

        let first_day = NaiveDate::from_ymd_opt(date.year(), date.month(), 1).unwrap();
        let mut current_date = first_day - Duration::days(first_day.weekday().num_days_from_sunday() as i64);

        for _ in 0..42 {
            if current_date.month() == date.month() {
                grid.push(Some(current_date));
            } else {
                grid.push(None);
            }
            current_date += Duration::days(1);
        }

        grid
    }

    fn draw_calendar_grid(
        &self,
        ui: &mut Ui,
        current_date: NaiveDate,
        selected_date: &mut NaiveDate,
    ) -> bool {
        let today = chrono::Local::now().date_naive();
        let mut changed = false;
        let grid = self.get_month_grid(current_date);

        Frame::none()
            .show(ui, |ui| {
                for week in grid.chunks(7) {
                    ui.horizontal(|ui| {
                        for &date_opt in week {
                            let (rect, response) = ui.allocate_exact_size(
                                vec2(30.0, 30.0),
                                Sense::click()
                            );

                            if let Some(date) = date_opt {
                                let is_selected = date == *selected_date;
                                let is_today = date == today;

                                // Draw background for selected/today/hovered dates
                                if is_selected {
                                    ui.painter().rect_filled(rect, 4.0, self.selected_color);
                                } else if is_today {
                                    ui.painter().rect_filled(rect, 4.0, self.today_color);
                                } else if response.hovered() {
                                    ui.painter().rect_filled(rect, 4.0, self.hover_color);
                                }

                                // Draw the date text
                                ui.painter().text(
                                    rect.center(),
                                    Align2::CENTER_CENTER,
                                    date.day().to_string(),
                                    egui::FontId::proportional(14.0),
                                    if is_selected {
                                        ui.visuals().strong_text_color()
                                    } else {
                                        ui.visuals().text_color()
                                    }
                                );

                                if response.clicked() {
                                    *selected_date = date;
                                    changed = true;
                                }
                            }
                        }
                    });
                }
            });

        changed
    }

    fn draw_spin_boxes(
        &self,
        ui: &mut Ui,
        selected_date: &mut NaiveDate,
    ) -> bool {
        let mut changed = false;
        let mut year = selected_date.year() as i32;
        let mut month = selected_date.month() as i32;
        let mut day = selected_date.day() as i32;

        ui.horizontal(|ui| {
            // Year spin box
            let mut year_spin = SpinBox::new()
                .width(50.0)
                .button_color(self.selected_color);
            if year_spin.show(ui, &mut year, 1, 9999, 1) {
                changed = true;
            }
            ui.label("/");

            // Month spin box
            let mut month_spin = SpinBox::new()
                .width(35.0)
                .button_color(self.selected_color);
            if month_spin.show(ui, &mut month, 1, 12, 1) {
                changed = true;
            }
            ui.label("/");

            // Day spin box
            let mut day_spin = SpinBox::new()
                .width(35.0)
                .button_color(self.selected_color);
            if day_spin.show(ui, &mut day, 1, 31, 1) {
                changed = true;
            }

            // Update the date if valid
            if changed {
                if let Some(new_date) = NaiveDate::from_ymd_opt(
                    year,
                    month as u32,
                    day as u32,
                ) {
                    *selected_date = new_date;
                }
            }
        });

        changed
    }

    pub fn show(&mut self, ui: &mut Ui, selected_date: &mut NaiveDate, shown_month: &mut NaiveDate) -> Response {
        let mut changed = false;

        // Center the entire widget horizontally
        ui.vertical(|ui| {
            // Constrain the maximum width
            ui.set_max_width(self.width);

            let mut child_response = Frame::none()
                .fill(ui.visuals().extreme_bg_color)
                .stroke(Stroke::new(1.0, ui.visuals().widgets.noninteractive.bg_stroke.color))
                .inner_margin(8.0)
                .rounding(8.0)
                .show(ui, |ui| {
                    ui.vertical_centered(|ui| {
                        // Draw spin boxes for direct date input
                        if self.draw_spin_boxes(ui, selected_date) {
                            changed = true;
                        }

                        ui.add_space(10.0);

                        // Draw calendar header with month/year and navigation buttons
                        if self.draw_calendar_header(ui, shown_month) {
                            // Only update selected date if day is valid in new month
                            if let Some(new_date) = NaiveDate::from_ymd_opt(
                                shown_month.year(),
                                shown_month.month(),
                                selected_date.day(),
                            ) {
                                *selected_date = new_date;
                                changed = true;
                            }
                        }

                        // Draw weekday headers
                        self.draw_weekday_headers(ui);

                        // Draw calendar grid
                        if self.draw_calendar_grid(ui, *shown_month, selected_date) {
                            changed = true;
                        }
                    });
                })
                .response;

            if changed {
                child_response.mark_changed();
            }

            child_response
        }).inner
    }
}