# Rosterator
Rosterator is a genetic algorithm based tool to generate rosters.

## Project structure
- application
  - Main user interface
  - Written in egui
- bidirectional_hash_map
  - Lookup from A to B and B to A
- qualified_id
  - Wrapper around UUIDs to provide more compile time check for where they are used.
- repository
  - Trait defining the repository pattern
  - Simple implementation of the trait