use std::collections::HashMap;
use std::hash::Hash;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct BidirectionalHashMap<A, B> where A: Eq + Hash + Clone, B: Eq + Hash + Clone {
    a_to_b: HashMap<A, B>,
    b_to_a: HashMap<B, A>,
}

impl<A, B> BidirectionalHashMap<A, B> where A: Eq + Hash + Clone, B: Eq + Hash + Clone {
    pub fn new() -> Self {
        Self {
            a_to_b: HashMap::new(),
            b_to_a: HashMap::new(),
        }
    }
    
    pub fn a_to_b(&self) -> &HashMap<A, B> {
        &self.a_to_b
    }

    pub fn b_to_a(&self) -> &HashMap<B, A> {
        &self.b_to_a
    }

    pub fn get_a_to_b(&self, key: &A) -> Option<&B> {
        self.a_to_b.get(key)
    }

    pub fn get_b_to_a(&self, key: &A) -> Option<&B> {
        self.a_to_b.get(key)
    }
    
    pub fn get_mut_a_to_b(&mut self, key: &A) -> Option<&mut B> {
        self.a_to_b.get_mut(key)
    }

    pub fn get_mut_b_to_a(&mut self, key: &A) -> Option<&mut B> {
        self.a_to_b.get_mut(key)
    }
    
    pub fn insert(&mut self, a: A, b: B) {
        self.a_to_b.insert(a.clone(), b.clone());
        self.b_to_a.insert(b, a);
    }
    
    pub fn contains_an_a(&self, a: &A) -> bool {
        self.a_to_b.contains_key(a)
    }
    
    pub fn contains_a_b(&self, b: &B) -> bool {
        self.b_to_a.contains_key(b)
    }
    
    pub fn remove_by_a_key(&mut self, a: &A) -> Option<B> {
        if let Some(b) = self.a_to_b.remove(a) {
            self.b_to_a.remove(&b);
            Some(b)
        } else {
            None
        }
    }

    pub fn remove_by_b_key(&mut self, b: &B) -> Option<A> {
        if let Some(a) = self.b_to_a.remove(b) {
            self.a_to_b.remove(&a);
            Some(a)
        } else {
            None
        }
    }
}