use std::hash::Hash;
use std::marker::PhantomData;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct QualifiedId<T> where T: Eq + Hash {
    marker: PhantomData<T>,
    id: Uuid,
}

impl<T> QualifiedId<T> where T: Eq + Hash {
    pub fn new() -> Self {
        Self {
            marker: PhantomData,
            id: Uuid::new_v4(),
        }
    }
}

impl<T> Clone for QualifiedId<T> where T: Eq + Hash {
    fn clone(&self) -> Self {
        *self
    }
}

impl<T> Copy for QualifiedId<T> where T: Eq + Hash {}