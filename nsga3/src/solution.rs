use std::collections::HashMap;
use crate::fitness::Fitness;
use crate::non_dominated_comparison_result::NonDominatedComparisonResult;
use crate::Objective;

pub trait Solution : Iterator<Item=Fitness> {
    fn generate_new() -> Self;

    fn fitness(&self) -> HashMap<Objective, impl PartialOrd>;
}

pub trait NonDominatedComparison {
    fn dominates(&self, other: Self) -> NonDominatedComparisonResult;
}

impl<T> NonDominatedComparison for T where T: Solution {
    fn dominates(&self, other: Self) -> NonDominatedComparisonResult {
        let mut dominated = false;

        for (v1, v2) in self.iter().zip(other.iter()) {
            if *v1 > *v2 {
                return NonDominatedComparisonResult::; // Solution 1 dominates
            } else if *v1 < *v2 {
                dominated = true;
            }
        }

        !dominated // If no dominance is found, then solution 2 dominates 
    }
}