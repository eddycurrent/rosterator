use crate::Solution;

pub struct Population<TSolution> where TSolution: Solution {
    solutions: Vec<TSolution>,
}

impl<TSolution> Population<TSolution> where TSolution: Solution {
    pub fn new(population_size: u64) -> Self {
        let solutions = (0..population_size).map(|_| TSolution::generate_new()).collect();
        Self {
            solutions,
        }
    }
}