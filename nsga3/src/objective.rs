#[derive(Clone, Debug)]
pub struct Objective {
    name: String,
}

impl Objective {
    pub fn new(name: impl Into<String>) -> Self {
        Self {
            name: name.into()
        }
    }

    pub fn name(&self) -> &str {
        &self.name
    }
}