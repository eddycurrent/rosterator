pub enum NonDominatedComparisonResult {
    NotDominated,
    Dominated,
    Dominates,
}