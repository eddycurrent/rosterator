use std::hash::{Hash, Hasher};

use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};

use crate::availability::Availability;
use crate::person::Person;
use crate::person_name::PersonName;
use crate::Generator;
use qualified_id::QualifiedId;
use crate::calendar_item_duration::CalendarItemDuration;
use crate::calendar_item_times::CalendarItemTimes;

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct AvailabilityDefinition {
    person_name: PersonName,
    person: QualifiedId<Person>,
    times: CalendarItemTimes,
    duration: CalendarItemDuration,
}

impl Hash for AvailabilityDefinition {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.person.hash(state);
        self.times.hash(state);
    }
}

impl AvailabilityDefinition {
    pub fn new(
        person_name: PersonName,
        person: QualifiedId<Person>,
        times: CalendarItemTimes,
        duration: CalendarItemDuration,
    ) -> Self {
        Self {
            person_name,
            person,
            times,
            duration,
        }
    }

    pub fn person(&self) -> QualifiedId<Person> {
        self.person
    }

    pub fn times(&self) -> &CalendarItemTimes {
        &self.times
    }

    pub fn remove_time(&self, time: NaiveDateTime) -> Option<Self> {
        self.times
            .remove_time(time)
            .map(|times| Self::new(self.person_name.clone(), self.person, times, self.duration))
    }

    pub fn generate_role_with_time(&self, time: NaiveDateTime) -> Availability {
        Availability::new(self.person_name.clone(), self.person, time, self.duration)
    }
}

impl Generator<Availability> for AvailabilityDefinition {
    fn first(&self) -> Option<Availability> {
        self.times
            .first()
            .map(|time| self.generate_role_with_time(time))
    }

    fn generate_next(&self, current: &Availability) -> Option<Availability> {
        self.times
            .generate_next(&current.time())
            .map(|time| self.generate_role_with_time(time))
    }
}
