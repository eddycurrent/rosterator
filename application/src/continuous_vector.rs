use crate::generator::Generator;

pub struct ContinuousVector<TSource, TGenerated>
where
    TSource: Generator<TGenerated>,
{
    source: TSource,
    data: Vec<TGenerated>,
}

impl<TSource, TGenerated> ContinuousVector<TSource, TGenerated>
where
    TSource: Generator<TGenerated>,
{
    pub fn new(source: TSource) -> Self {
        let data = source
            .first()
            .map_or_else(|| Vec::new(), |value| vec![value]);
        Self { source, data }
    }
}

// TODO: this might be a little disingenuous
impl<TSource, TGenerated> IntoIterator for ContinuousVector<TSource, TGenerated>
where
    TSource: Generator<TGenerated>,
{
    type Item = TGenerated;
    type IntoIter = std::vec::IntoIter<Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.data.into_iter()
    }
}

/*impl<TSource, TGenerated, StopState> ContinuousGenerator<TSource, TGenerated, StopState> for ContinuousVector<TSource, TGenerated> where TSource: Generator<TGenerated>, TGenerated: Clone + GeneratorStopPredicate<StopState>, StopState: Copy + PartialOrd {
    fn generate_until(&mut self, stop_state: &StopState) {
        let mut next = self.source.generate_next(&self.data[self.data.len() - 1]);
        while let Some(value) = next && !value.stop(stop_state) {
            next = self.source.generate_next(&value);
            self.data.push(value.clone());
        }
    }
}*/
