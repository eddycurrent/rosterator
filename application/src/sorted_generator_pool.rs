/*use itertools::Itertools;
use serde::{Deserialize, Serialize};

use crate::continuous_generator::ContinuousGenerator;
use crate::continuous_vector::ContinuousVector;
use crate::generator::Generator;
use crate::generator_predicate::GeneratorStopPredicate;
use crate::windowed_vector::WindowedVector;

#[derive(Serialize, Deserialize)]
pub struct SortedGeneratorPool<TSource, TGenerated, StopState> where TSource: Generator<TGenerated>, TGenerated: Ord + GeneratorStopPredicate<StopState>, StopState: Copy + PartialOrd {
    sources: Vec<TSource>,
    data: Vec<TGenerated>,
    stop_state: StopState,
}

impl<TSource, TGenerated, StopState> SortedGeneratorPool<TSource, TGenerated, StopState> where TSource: Generator<TGenerated> + Clone, TGenerated: Clone + Ord + GeneratorStopPredicate<StopState>, StopState: Default + Copy + PartialOrd {
    pub fn new() -> Self {
        Self {
            sources: Vec::new(),
            data: Vec::new(),
            stop_state: StopState::default()
        }
    }

    pub fn add_source(&mut self, source: TSource) {
        self.sources.push(source);
        self.update();
    }

    pub fn change_end_condition(&mut self, stop_state: StopState) {
        self.stop_state = stop_state;
        self.update();
    }

    fn update(&mut self) {
        // TODO: speed this up
        self.data = self.sources
            .clone()
            .into_iter()
            .flat_map(|source| {
                let mut vec = ContinuousVector::new(source.clone());
                vec.generate_until(&self.stop_state);
                vec
            })
            .sorted()
            .collect();
    }

    /*pub fn remove_where(& mut self, mut filter: impl FnMut(&TGenerated) -> bool) {
        self.sources = self.sources.clone().into_iter().filter(|value| !filter(value)).collect();
        self.update();
    }*/

    pub fn sources(&self) -> &Vec<TSource> {
        &self.sources
    }
}

impl<TSource, TGenerated, StopState> ContinuousGenerator<TSource, TGenerated, StopState> for SortedGeneratorPool<TSource, TGenerated, StopState> where TSource: Generator<TGenerated> + Clone, TGenerated: Clone + Ord + GeneratorStopPredicate<StopState>, StopState: Default + Copy + PartialOrd {
    fn generate_until(&mut self, stop_state: &StopState) {
        self.change_end_condition(*stop_state);
    }
}

impl<TSource, TGenerated, State> WindowedVector<TGenerated, State> for SortedGeneratorPool<TSource, TGenerated, State> where TSource: Generator<TGenerated> + Clone, TGenerated: Ord + GeneratorStopPredicate<State> + Clone, State: Default + Copy + PartialOrd {
    fn get_window(&mut self, start: fn(&TGenerated, &State) -> bool, start_state: &State, end: fn(&TGenerated, &State) -> bool, end_state: &State) -> Vec<TGenerated> {
        self.generate_until(end_state);

        let mut output = Vec::new();

        let mut take = false;

        for item in &self.data {
            if !take {
                if start(item, start_state) {
                    take = true;
                    output.push(item.clone());
                }
            }
            else if end(item, end_state) {
                break;
            }
            else {
                output.push(item.clone());
            }
        }

        output
    }
}

impl<TSource, TGenerated, State> FromIterator<TSource> for SortedGeneratorPool<TSource, TGenerated, State> where TSource: Generator<TGenerated> + Clone, TGenerated: Clone + Ord + GeneratorStopPredicate<State>, State: Default + Copy + PartialOrd {
    fn from_iter<U: IntoIterator<Item=TSource>>(iter: U) -> Self {
        let mut output = Self::new();
        for item in iter {
            output.add_source(item)
        }
        output
    }
}*/
