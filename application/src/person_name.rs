use eframe::egui::WidgetText;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialOrd, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct PersonName {
    value: String,
}

impl PersonName {
    pub fn new<S>(value: S) -> Self
    where
        S: Into<String>,
    {
        Self {
            value: value.into(),
        }
    }

    pub fn value(&self) -> String {
        self.value.clone()
    }
}

impl<T> From<T> for PersonName
where
    T: Into<String>,
{
    fn from(value: T) -> Self {
        Self::new(value)
    }
}

impl From<PersonName> for WidgetText {
    fn from(value: PersonName) -> Self {
        WidgetText::from(value.value)
    }
}
