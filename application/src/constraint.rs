use crate::role_mutual_exclusion_constraint::RoleMutualExclusionConstraint;
use crate::roster::Roster;
use itertools::Itertools;

#[derive(Clone, Debug)]
pub enum Constraint {
    RoleMutualExclusionConstraint(RoleMutualExclusionConstraint),
}

impl Constraint {
    pub fn violated(&self, roster: &Roster) -> bool {
        match self {
            Self::RoleMutualExclusionConstraint(constraint) => roster
                .assignments()
                .iter()
                .group_by(|assignment| assignment.person().clone())
                .into_iter()
                .any(|group| {
                    let mut roles = group.1.map(|assigment| assigment.role().title().clone());
                    roles.contains(&constraint.role_a()) && roles.contains(&constraint.role_b())
                }),
        }
    }
}

impl From<RoleMutualExclusionConstraint> for Constraint {
    fn from(value: RoleMutualExclusionConstraint) -> Self {
        Constraint::RoleMutualExclusionConstraint(value)
    }
}
