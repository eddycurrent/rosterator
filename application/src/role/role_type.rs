use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialOrd, PartialEq, Eq, Ord, Hash, Serialize, Deserialize)]
pub struct RoleType {
    value: String,
    description: String,
}

impl RoleType {
    pub fn new(value: impl Into<String>, description: impl Into<String>) -> Self {
        Self {
            value: value.into(),
            description: description.into(),
        }
    }

    pub fn value(&self) -> String {
        self.value.clone()
    }

    pub fn description(&self) -> String {
        self.description.clone()
    }

    pub fn with_type(&self, r#type: String) -> Self {
        Self::new(r#type, self.description.clone())
    }

    pub fn with_description(&self, description: String) -> Self {
        Self::new(self.value.clone(), description)
    }
}

pub mod tests {
    use super::RoleType;

    #[test]
    pub fn eq() {
        assert_eq!(
            RoleType::new("type", "description"),
            RoleType::new("type", "description")
        )
    }
}
