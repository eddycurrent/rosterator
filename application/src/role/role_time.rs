use std::ops::Add;

use crate::calendar_item_repetition::CalendarItemRepetition;
use chrono::{Duration, NaiveDateTime};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct RoleTime {
    time: NaiveDateTime,
    repeats: CalendarItemRepetition,
}

impl RoleTime {
    pub fn new(time: NaiveDateTime, repeats: CalendarItemRepetition) -> Self {
        Self { time, repeats }
    }

    pub fn time(&self) -> NaiveDateTime {
        self.time
    }

    pub fn repeats(&self) -> CalendarItemRepetition {
        self.repeats
    }

    pub fn offset(&self, offset: Duration) -> Self {
        Self::new(self.time.add(offset), self.repeats)
    }

    pub fn offset_and_decrement(&self, offset: Duration) -> Option<Self> {
        match self.repeats {
            CalendarItemRepetition::None => Some(self.offset(offset)),
            CalendarItemRepetition::Weekly { count } => match count {
                Some(count) => {
                    if count > 1 {
                        Some(Self::new(
                            self.time.add(offset),
                            CalendarItemRepetition::Weekly {
                                count: Some(count - 1),
                            },
                        ))
                    } else {
                        None
                    }
                }
                None => Some(self.offset(offset)),
            },
        }
    }
}
