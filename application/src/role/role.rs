use std::cmp::Ordering;

use chrono::{NaiveDate, NaiveDateTime};
use serde::{Deserialize, Serialize};

use qualified_id::QualifiedId;
use crate::calendar_item_duration::CalendarItemDuration;
use crate::generator_predicate::GeneratorStopPredicate;
use crate::role::{RoleImportance, RoleTitle, RoleType};

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Role {
    title: RoleTitle,
    r#type: QualifiedId<RoleType>,
    time: NaiveDateTime,
    duration: CalendarItemDuration,
    importance: RoleImportance,
}

impl Role {
    pub fn new(
        title: impl Into<RoleTitle>,
        r#type: QualifiedId<RoleType>,
        time: NaiveDateTime,
        duration: CalendarItemDuration,
        importance: RoleImportance,
    ) -> Self {
        Self {
            title: title.into(),
            r#type,
            time,
            duration,
            importance,
        }
    }

    pub fn title(&self) -> RoleTitle {
        self.title.clone()
    }

    pub fn r#type(&self) -> QualifiedId<RoleType> {
        self.r#type
    }

    pub fn time(&self) -> NaiveDateTime {
        self.time
    }

    pub fn duration(&self) -> CalendarItemDuration {
        self.duration
    }

    pub fn overlaps(&self, other: &Self) -> bool {
        (self.time <= other.time && self.time + self.duration.duration() > other.time)
            || (other.time <= self.time && other.time + other.duration.duration() > self.time)
    }
}

impl PartialOrd for Role {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.time.time().partial_cmp(&other.time.time())
    }
}

impl Ord for Role {
    fn cmp(&self, other: &Self) -> Ordering {
        self.time.time().cmp(&other.time.time())
    }
}

impl GeneratorStopPredicate<NaiveDate> for Role {
    fn stop(&self, value: &NaiveDate) -> bool {
        self.time.date() >= *value
    }
}
