use std::hash::{Hash, Hasher};

use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};

use qualified_id::QualifiedId;
use crate::calendar_item_duration::CalendarItemDuration;
use crate::calendar_item_times::CalendarItemTimes;
use crate::role::{Role, RoleImportance, RoleTitle, RoleType};
use crate::Generator;

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct RoleDefinition {
    title: RoleTitle,
    r#type: QualifiedId<RoleType>,
    times: CalendarItemTimes,
    duration: CalendarItemDuration,
    importance: RoleImportance,
}

impl Hash for RoleDefinition {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.title.hash(state);
    }
}

impl RoleDefinition {
    pub fn new(
        title: impl Into<RoleTitle>,
        r#type: QualifiedId<RoleType>,
        times: CalendarItemTimes,
        duration: CalendarItemDuration,
        importance: RoleImportance,
    ) -> Self {
        Self {
            title: title.into(),
            r#type,
            times,
            duration,
            importance,
        }
    }

    pub fn title(&self) -> RoleTitle {
        self.title.clone()
    }

    pub fn r#type(&self) -> QualifiedId<RoleType> {
        self.r#type
    }

    pub fn times(&self) -> &CalendarItemTimes {
        &self.times
    }

    pub fn remove_time(&self, time: NaiveDateTime) -> Option<Self> {
        self.times.remove_time(time).map(|times| {
            Self::new(
                self.title.clone(),
                self.r#type,
                times,
                self.duration,
                self.importance,
            )
        })
    }

    pub fn generate_role_with_time(&self, time: NaiveDateTime) -> Role {
        Role::new(
            self.title.clone(),
            self.r#type,
            time,
            self.duration.clone(),
            self.importance.clone(),
        )
    }
}

impl Generator<Role> for RoleDefinition {
    fn first(&self) -> Option<Role> {
        self.times
            .first()
            .map(|time| self.generate_role_with_time(time))
    }

    fn generate_next(&self, current: &Role) -> Option<Role> {
        self.times
            .generate_next(&current.time())
            .map(|time| self.generate_role_with_time(time))
    }
}
