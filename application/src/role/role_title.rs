use eframe::egui::WidgetText;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialOrd, PartialEq, Eq, Ord, Hash, Serialize, Deserialize)]
pub struct RoleTitle {
    value: String,
}

impl RoleTitle {
    pub fn new<S>(value: S) -> Self
    where
        S: Into<String>,
    {
        Self {
            value: value.into(),
        }
    }
}

impl RoleTitle {
    pub fn value(&self) -> &String {
        &self.value
    }
}

impl<T> From<T> for RoleTitle
where
    T: Into<String>,
{
    fn from(value: T) -> Self {
        Self::new(value)
    }
}

impl From<RoleTitle> for WidgetText {
    fn from(value: RoleTitle) -> Self {
        WidgetText::from(value.value)
    }
}
