use eframe::emath::Align;
use eframe::epaint::Color32;
use egui::{Button, Grid, Layout, ScrollArea, Ui};
use egui_extras::{Column, TableBuilder};
use itertools::Itertools;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;

use qualified_id::QualifiedId;
use repository::{Repository, SimpleRepository};

use crate::person::Person;
use crate::PersonRowViewModel;
use crate::role::RoleType;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct PeoplePageViewModel {
    people: Vec<PersonRowViewModel>,
    new_person_name: String,
    new_person_roles: HashSet<QualifiedId<RoleType>>,
}

impl PeoplePageViewModel {
    pub fn new() -> Self {
        Self {
            people: Vec::new(),
            new_person_name: String::new(),
            new_person_roles: HashSet::new(),
        }
    }

    pub fn render(
        &mut self,
        ui: &mut Ui,
        people: &mut SimpleRepository<Person>,
        role_types: &SimpleRepository<RoleType>,
    ) {
        ui.with_layout(Layout::right_to_left(Align::TOP), |ui| {
            ui.hyperlink_to(
                "Need help?",
                "https://gitlab.com/eddycurrent/rosterator/-/wikis/concepts/person",
            );
        });

        let title_column_width = (ui.available_width() / 6.0).min(150.0);
        let description_column_width = ui.available_width() - title_column_width - 150.0;
        let action_column_width = 150.0;
        TableBuilder::new(ui)
            .column(Column::exact(title_column_width))
            .column(Column::exact(description_column_width))
            .column(Column::exact(action_column_width))
            .striped(true)
            .header(20.0, |mut header| {
                header.col(|ui| {
                    ui.label("Name");
                });
                header.col(|ui| {
                    ui.label("Roles");
                });
            })
            .body(|mut body| {
                for id in people.keys() {
                    if self
                        .people
                        .iter()
                        .all(|view_model| view_model.person_id() != id)
                    {
                        self.people.push(PersonRowViewModel::new(id));
                    }
                }
                self.people = self
                    .people
                    .iter()
                    .filter_map(|person_row_view_model| {
                        person_row_view_model.render(&mut body, people, role_types)
                    })
                    .collect();

                body.row(50.0, |mut row| {
                    row.col(|ui| {
                        ui.text_edit_singleline(&mut self.new_person_name);
                    });
                    row.col(|ui| {
                        ScrollArea::vertical()
                            .max_width(ui.available_width())
                            .show(ui, |ui| {
                                Grid::new("new person roles").show(ui, |ui| {
                                    for role_type_chunk in &role_types.keys().iter().chunks(4) {
                                        for role_type in role_type_chunk {
                                            let mut checked =
                                                self.new_person_roles.contains(&role_type);
                                            let original_value = checked;
                                            // TODO: Handle unwrap
                                            ui.checkbox(
                                                &mut checked,
                                                role_types.get(role_type).unwrap().value(),
                                            );
                                            if checked != original_value {
                                                if checked {
                                                    self.new_person_roles.insert(role_type.clone());
                                                } else {
                                                    self.new_person_roles.remove(role_type);
                                                }
                                            }
                                        }
                                        ui.end_row();
                                    }
                                });
                            });
                    });
                    row.col(|ui| {
                        let add_button = Button::new("Add").fill(Color32::DARK_GREEN);
                        let add_button_response = ui.add(add_button);
                        if add_button_response.clicked() {
                            let id = people.add(Person::new(
                                self.new_person_name.clone(),
                                self.new_person_roles.clone(),
                            ));
                            self.people.push(PersonRowViewModel::new(id));
                            self.new_person_name = String::new();
                            self.new_person_roles = HashSet::new();
                        }
                    });
                });
            });
    }
}
