use crate::availability::Availability;
use crate::constraint::Constraint;
use crate::person::Person;
use crate::person_name::PersonName;
use crate::role::{Role, RoleType};
use itertools::Itertools;
use qualified_id::QualifiedId;
use std::collections::HashMap;

#[derive(Clone, Debug)]
pub struct MainConfiguration {
    population_count: usize,
    constraints: Vec<Constraint>,
    roles: Vec<Role>,
    people: HashMap<PersonName, Person>,
    availabilities: Vec<Availability>,
}

impl MainConfiguration {
    pub fn new<C, D, E, F, G, H, I, J>(
        population_count: usize,
        constraints: C,
        roles: E,
        people: G,
        availabilities: I,
    ) -> Result<Self, Vec<String>>
    where
        C: IntoIterator<Item = D>,
        D: Into<Constraint>,
        E: IntoIterator<Item = F>,
        F: Into<Role>,
        G: IntoIterator<Item = H>,
        H: Into<Person>,
        I: IntoIterator<Item = J>,
        J: Into<Availability>,
    {
        let mut validation = Vec::new();

        let constraints = constraints
            .into_iter()
            .map(|constraint| constraint.into())
            .collect();

        let roles: Vec<Role> = roles.into_iter().map(|role| role.into()).collect();
        if roles.is_empty() {
            validation.push("Must have at least one role".to_string());
        }

        let people: HashMap<PersonName, Person> = people
            .into_iter()
            .map(|constraint| {
                let converted = constraint.into();
                (converted.name(), converted)
            })
            .collect();

        if people.is_empty() {
            validation.push("Must have at least one person".to_string());
        }

        if !validation.is_empty() {
            Err(validation)
        } else {
            Ok(Self {
                population_count,
                constraints,
                roles,
                people,
                availabilities: availabilities
                    .into_iter()
                    .map(|availability| availability.into())
                    .collect(),
            })
        }
    }

    pub fn population_count(&self) -> usize {
        self.population_count
    }

    pub fn role_count(&self) -> usize {
        self.roles.len()
    }

    pub fn roles(&self) -> Vec<Role> {
        self.roles.clone()
    }

    pub fn constraint_count(&self) -> usize {
        self.constraints.len()
    }

    pub fn constraints(&self) -> Vec<Constraint> {
        self.constraints.clone()
    }

    pub fn people(&self) -> Vec<Person> {
        self.people.values().cloned().collect()
    }

    pub fn person(&self, name: &PersonName) -> Person {
        self.people[name].clone()
    }

    pub fn availabilities(&self) -> &Vec<Availability> {
        &self.availabilities
    }

    pub fn possible_people_per_role_type(&self) -> HashMap<QualifiedId<RoleType>, u64> {
        self.roles
            .iter()
            .map(|role| role.r#type())
            .unique()
            .map(|role_type| {
                (
                    role_type,
                    self.people
                        .values()
                        .filter(|person| person.has_capability(&role_type))
                        .count() as u64,
                )
            })
            .collect()
    }
}
