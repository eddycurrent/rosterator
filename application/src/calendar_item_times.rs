use crate::calendar_item_repetition::CalendarItemRepetition;
use crate::generator::Generator;
use chrono::{NaiveDateTime, TimeDelta};
use egui::ahash::HashSet;
use serde::{Deserialize, Serialize};
use std::hash::{Hash, Hasher};
use std::ops::Add;

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct CalendarItemTimes {
    initial: NaiveDateTime,
    deleted: HashSet<NaiveDateTime>,
    repeats: CalendarItemRepetition,
}

impl CalendarItemTimes {
    pub fn new(time: NaiveDateTime, repeats: CalendarItemRepetition) -> Self {
        Self::from_components(time, HashSet::default(), repeats)
    }

    fn from_components(
        initial: NaiveDateTime,
        deleted: HashSet<NaiveDateTime>,
        repeats: CalendarItemRepetition,
    ) -> Self {
        Self {
            initial,
            deleted,
            repeats,
        }
    }

    fn with_deleted_time(&self, deleted: NaiveDateTime) -> Self {
        let mut new_deleted = self.deleted.clone();
        new_deleted.insert(deleted);
        Self::from_components(self.initial, new_deleted, self.repeats)
    }

    pub fn repeats(&self) -> CalendarItemRepetition {
        self.repeats
    }

    pub fn remove_time(&self, time: NaiveDateTime) -> Option<Self> {
        match self.repeats {
            CalendarItemRepetition::None => {
                if self.initial == time {
                    None
                } else {
                    Some(self.clone())
                }
            }
            CalendarItemRepetition::Weekly { count } => {
                match count {
                    None => {
                        // TODO: only remove actually included times.
                        Some(self.with_deleted_time(time))
                    }
                    Some(_) => {
                        let with_deleted = self.with_deleted_time(time);
                        with_deleted.first().map(|_| with_deleted)
                    }
                }
            }
        }
    }
}

impl Generator<NaiveDateTime> for CalendarItemTimes {
    fn first(&self) -> Option<NaiveDateTime> {
        match self.repeats {
            CalendarItemRepetition::None => {
                if self.deleted.contains(&self.initial) {
                    None
                } else {
                    Some(self.initial)
                }
            }
            CalendarItemRepetition::Weekly { count } => match count {
                None => {
                    let mut time = self.initial;
                    while self.deleted.contains(&time) {
                        time = time.add(TimeDelta::weeks(1));
                    }
                    Some(time)
                }
                Some(count) => {
                    let mut time = self.initial;
                    let mut generated_count = 1;
                    while self.deleted.contains(&time) && generated_count <= count {
                        time = time.add(TimeDelta::weeks(1));
                        generated_count += 1;
                    }

                    if generated_count > count {
                        None
                    } else {
                        Some(time)
                    }
                }
            },
        }
    }

    fn generate_next(&self, current: &NaiveDateTime) -> Option<NaiveDateTime> {
        match self.repeats {
            CalendarItemRepetition::None => None,
            CalendarItemRepetition::Weekly { count } => match count {
                None => {
                    let mut time = current.add(TimeDelta::weeks(1));
                    while self.deleted.contains(&time) {
                        time = time.add(TimeDelta::weeks(1));
                    }
                    Some(time)
                }
                Some(count) => {
                    let mut time = current.add(TimeDelta::weeks(1));
                    while self.deleted.contains(&time)
                        && time < self.initial.add(TimeDelta::weeks(count as i64))
                    {
                        time = time.add(TimeDelta::weeks(1));
                    }

                    if time >= self.initial.add(TimeDelta::weeks(count as i64)) {
                        None
                    } else {
                        Some(time)
                    }
                }
            },
        }
    }
}

impl Hash for CalendarItemTimes {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.initial.hash(state);
        for deleted in &self.deleted {
            deleted.hash(state);
        }
        self.repeats.hash(state);
    }
}
