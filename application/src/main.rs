#![feature(let_chains)]

extern crate core;

use std::default::Default;

use eframe::egui;
use eframe::egui::{vec2, Button, Rounding, TextStyle, Ui, ViewportBuilder};
use eframe::emath::Rect;
use egui::{Style, Visuals};
use itertools::Itertools;
use rand::{thread_rng, Rng};
use rayon::iter::IntoParallelIterator;
use rayon::prelude::*;
use serde::{Deserialize, Serialize};

use crate::algorithm_progress::AlgorithmProgress;
use crate::application_state::ApplicationState;
use crate::roster::Roster;
use generator::Generator;
use individual::Individual;
use main_configuration::MainConfiguration;
pub use person_row_view_model::PersonRowViewModel;

mod add_availability_window_state;
mod add_role_window_state;
mod algorithm_progress;
mod application_state;
mod assignment;
mod availability;
mod availability_definition;
mod calendar_item_duration;
mod calendar_item_repetition;
mod calendar_item_times;
mod constraint;
mod continuous_generator;
mod continuous_vector;
mod cuttable_generator;
mod generator;
mod generator_predicate;
mod individual;
mod main_configuration;
mod people_page_view_model;
mod person;
mod person_name;
mod person_row_view_model;
mod project;
mod role;
mod role_mutual_exclusion_constraint;
mod role_type_page_view_model;
mod role_type_row_view_model;
mod roster;
mod sorted_generator_pool;
mod windowed_vector;
mod colors;

fn main() -> Result<(), eframe::Error> {
    // Log to stderr (if you run with `RUST_LOG=debug`).
    env_logger::init();
    let options = eframe::NativeOptions {
        viewport: ViewportBuilder::default()
            .with_inner_size(vec2(1000.0, 800.0))
            .with_maximized(true)
            .with_resizable(true),
        ..Default::default()
    };

    eframe::run_native(
        "Rosterator",
        options,
        Box::new(|cc| {
            // This gives us image support:
            egui_extras::install_image_loaders(&cc.egui_ctx);
            cc.egui_ctx.set_style(Style {
                visuals: Visuals::dark(),
                ..Style::default()
            });
            Ok(Box::<ApplicationState>::default())
        }),
    )
}

#[derive(Eq, PartialEq, Serialize, Deserialize)]
pub enum Page {
    RoleTypes,
    Roles,
    People,
    Availability,
    Constraints,
    AlgorithmParameters,
    Run,
    View,
}

impl Page {
    pub fn to_title_string(&self) -> String {
        match self {
            Self::RoleTypes => String::from("Role Types"),
            Self::Roles => String::from("Roles"),
            Self::People => String::from("People"),
            Self::Availability => String::from("Availability"),
            Self::Constraints => String::from("Constraints"),
            Self::AlgorithmParameters => String::from("Algorithm Parameters"),
            Self::Run => String::from("Run"),
            Self::View => String::from("View"),
        }
    }
}

fn plus_button(ui: &mut Ui, on_clicked: impl FnOnce()) {
    ui.style_mut().override_text_style = Some(TextStyle::Heading);
    let button = Button::new("+")
        .min_size(vec2(30.0, 30.0))
        .rounding(Rounding::same(15.0));
    let person_add_button_response = ui.put(
        Rect::from_two_pos(
            ui.max_rect().max - vec2(35.0, 35.0),
            ui.max_rect().max - vec2(5.0, 5.0),
        ),
        button,
    );
    if person_add_button_response.clicked() {
        on_clicked();
    }
}

fn run_ga(
    main_configuration: &MainConfiguration,
    on_progress: impl Fn(AlgorithmProgress),
) -> Roster {
    // TODO: revert to random?
    let mut population: Vec<_> = (0..main_configuration.population_count())
        .into_par_iter()
        .map(|_| {
            if thread_rng().gen_bool(0.5) {
                Individual::new_randomized(main_configuration)
            } else {
                Individual::new_randomized_sequential(main_configuration)
            }
        })
        .collect();

    let mut generation = 0;

    let max_generations = 1000;
    let to_keep = 300;

    let mut kept = Vec::new();
    let mut best = None;

    while generation < max_generations {
        let progress = generation as f32 / max_generations as f32;
        on_progress(best.map_or_else(
            || AlgorithmProgress::new(progress, 0, vec![0, 0, 0, 0, 0, 0]),
            |individual: Individual| {
                let roster = individual.roster();
                let consecutive_counts = roster.consecutive_counts();
                AlgorithmProgress::new(
                    progress,
                    individual.roles_filled(),
                    consecutive_counts.clone(),
                )
            },
        ));
        population.par_sort_by_cached_key(|individual| individual.roster().consecutive_counts()[5]);
        population.par_sort_by_cached_key(|individual| individual.roster().consecutive_counts()[4]);
        population.par_sort_by_cached_key(|individual| individual.roster().consecutive_counts()[3]);
        population.par_sort_by_cached_key(|individual| individual.roster().consecutive_counts()[2]);
        population.par_sort_by_cached_key(|individual| individual.roster().consecutive_counts()[1]);
        population.par_sort_by_cached_key(|individual| individual.roster().consecutive_counts()[0]);
        population.reverse();
        population.par_sort_by_key(|individual| individual.roles_filled());
        population.reverse();
        kept.append(&mut population.iter().take(100).cloned().collect_vec());

        population.reverse();
        population.par_sort_by_key(|individual| individual.roles_filled());
        population.reverse();
        population.par_sort_by_cached_key(|individual| individual.roster().consecutive_counts()[0]);
        kept.append(&mut population.iter().take(100).cloned().collect_vec());

        population.par_sort_by_cached_key(|individual| individual.roster().consecutive_counts()[0]);
        population.reverse();
        population.par_sort_by_key(|individual| individual.roles_filled());
        population.reverse();
        population.par_sort_by_cached_key(|individual| individual.roster().consecutive_counts()[1]);
        kept.append(&mut population.iter().take(100).cloned().collect_vec());

        best = Some(kept[0].clone());

        let to_generate = main_configuration.population_count() - to_keep;

        population = (0..to_generate)
            .into_par_iter()
            .map(|_| {
                let mut rng = thread_rng();
                if rng.gen_bool(0.5) {
                    let index = rng.gen_range(0..main_configuration.population_count());
                    population[index].mutate(&main_configuration)
                } else {
                    let index1 = rng.gen_range(0..main_configuration.population_count());
                    let index2 = rng.gen_range(0..main_configuration.population_count());
                    population[index1].crossover(&main_configuration, &population[index2])
                }
            })
            .collect();

        population.append(&mut kept);

        generation += 1;
    }

    population.par_sort_by_cached_key(|individual| individual.roster().consecutive_counts()[1]);
    population.par_sort_by_cached_key(|individual| individual.roster().consecutive_counts()[0]);
    population.par_sort_by_key(|individual| individual.roles_filled());
    population.reverse();

    let best = population[0].clone();
    println!("{}", best.roster().consecutive_counts()[0]);
    println!("{}", best.roster().consecutive_counts()[1]);

    best.roster()
}
