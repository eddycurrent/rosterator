use crate::role::RoleTitle;

#[derive(Clone, Debug)]
pub struct RoleMutualExclusionConstraint {
    role_a: RoleTitle,
    role_b: RoleTitle,
}

impl RoleMutualExclusionConstraint {
    pub fn new<T>(role_a: T, role_b: T) -> Self
    where
        T: Into<RoleTitle>,
    {
        Self {
            role_a: role_a.into(),
            role_b: role_b.into(),
        }
    }

    pub fn role_a(&self) -> RoleTitle {
        self.role_a.clone()
    }

    pub fn role_b(&self) -> RoleTitle {
        self.role_b.clone()
    }
}
