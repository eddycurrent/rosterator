pub trait WindowedVector<T, STATE> {
    // TODO: investigate removing this mut
    fn get_window(
        &mut self,
        start: fn(&T, &STATE) -> bool,
        start_state: &STATE,
        end: fn(&T, &STATE) -> bool,
        end_state: &STATE,
    ) -> Vec<T>;
}
