use crate::generator::Generator;

pub trait CuttableGenerator<T>: Generator<T>
where
    Self: Sized,
{
    fn cut_before(&self, cut_point: &T) -> (Option<Self>, Option<Self>);
}
