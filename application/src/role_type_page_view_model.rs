use std::collections::HashMap;

use eframe::emath::Align;
use eframe::epaint::Color32;
use egui::ahash::HashSet;
use egui::{Button, Ui};
use egui_commonmark::CommonMarkCache;
use egui_extras::{Column, TableBuilder};
use itertools::Itertools;
use serde::{Deserialize, Serialize};

use repository::{Repository, SimpleRepository};
use crate::role::RoleType;
use crate::role_type_row_view_model::RoleTypeRowViewModel;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct RoleTypePageViewModel {
    role_types: Vec<RoleTypeRowViewModel>,
    type_edits: HashMap<RoleType, String>,
    description_edits: HashMap<RoleType, String>,
    new_role_type_type: String,
    new_role_type_description: String,
}

impl RoleTypePageViewModel {
    pub fn new() -> Self {
        Self {
            role_types: Vec::new(),
            type_edits: HashMap::new(),
            description_edits: HashMap::new(),
            new_role_type_type: String::new(),
            new_role_type_description: String::new(),
        }
    }

    pub fn render(
        &mut self,
        ui: &mut Ui,
        role_types: &mut SimpleRepository<RoleType>,
        cache: &mut CommonMarkCache,
    ) {
        let title_column_width = (ui.available_width() / 6.0).min(150.0);
        let action_column_width = 150.0;

        ui.with_layout(egui::Layout::right_to_left(Align::TOP), |ui| {
            ui.hyperlink_to(
                "Need help?",
                "https://gitlab.com/eddycurrent/rosterator/-/wikis/concepts/role-type",
            );
        });

        TableBuilder::new(ui)
            .column(Column::exact(title_column_width))
            .column(Column::remainder())
            .column(Column::exact(action_column_width))
            .striped(true)
            .header(20.0, |mut header| {
                header.col(|ui| {
                    ui.label("Role Type");
                });
                header.col(|ui| {
                    ui.label("Description");
                });
            })
            .body(|mut body| {
                for id in role_types.keys() {
                    if self
                        .role_types
                        .iter()
                        .all(|view_model| view_model.role_type_id() != id)
                    {
                        self.role_types.push(RoleTypeRowViewModel::new(id));
                    }
                }

                self.role_types = self
                    .role_types
                    .iter_mut()
                    .filter_map(|role_type_row_view_model| {
                        role_type_row_view_model.render(&mut body, role_types, cache)
                    })
                    .collect();

                // TODO: remove this double assignment
                self.role_types.sort_by_key(|role_type_page_view_model| {
                    role_types
                        .get(&role_type_page_view_model.role_type_id())
                        .unwrap()
                        .value()
                });

                let retained_ids: HashSet<_> =
                    self.role_types.iter().map(|r| r.role_type_id()).collect();
                let keys = role_types.keys();
                let deleted_ids = keys
                    .iter()
                    .filter(|id| !retained_ids.contains(id))
                    .collect_vec();

                for id in deleted_ids {
                    role_types.remove(id);
                }

                body.row(70.0, |mut row| {
                    let mut is_valid = false;
                    row.col(|ui| {
                        ui.text_edit_singleline(&mut self.new_role_type_type);
                        let is_empty = self.new_role_type_type.is_empty();
                        if is_empty {
                            ui.label("Cannot be empty");
                        }
                        let is_repeated = role_types
                            .values()
                            .iter()
                            .any(|role_type| role_type.value() == self.new_role_type_type);
                        if is_repeated {
                            ui.label("Cannot be the same as another role type");
                        }

                        is_valid = !is_empty && !is_repeated;
                    });
                    row.col(|ui| {
                        ui.text_edit_multiline(&mut self.new_role_type_description);
                    });
                    row.col(|ui| {
                        if is_valid {
                            ui.disable();
                        }
                        let add_button = Button::new("Add").fill(Color32::DARK_GREEN);
                        let add_button_response = ui.add(add_button);
                        if add_button_response.clicked() {
                            let id = role_types.add(RoleType::new(
                                &self.new_role_type_type,
                                &self.new_role_type_description,
                            ));
                            self.role_types.push(RoleTypeRowViewModel::new(id));
                            self.new_role_type_type = String::new();
                            self.new_role_type_description = String::new();
                        }
                    });
                });
            });
    }
}
