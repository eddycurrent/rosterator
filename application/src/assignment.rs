use crate::person_name::PersonName;
use crate::role::Role;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Assignment {
    person: PersonName,
    role: Role,
}

impl Assignment {
    pub fn new(person: PersonName, role: Role) -> Self {
        Self { person, role }
    }

    pub fn person(&self) -> PersonName {
        self.person.clone()
    }

    pub fn role(&self) -> Role {
        self.role.clone()
    }
}
