use egui::ahash::HashMap;
use itertools::Itertools;
use rand::seq::SliceRandom;
use rand::{thread_rng, Rng};

use qualified_id::QualifiedId;

use crate::assignment::Assignment;
use crate::main_configuration::MainConfiguration;
use crate::person_name::PersonName;
use crate::role::RoleType;
use crate::roster::Roster;

#[derive(Clone, Debug)]
pub struct Individual {
    people: Vec<PersonName>,
    roster: Roster,
    roles_for_busiest_person: u64,
    constraints_met: u64,
}

impl Individual {
    pub fn new_randomized(main_configuration: &MainConfiguration) -> Self {
        let mut rng = thread_rng();

        let mut people = Vec::new();

        let mut assigmnents: Vec<Assignment> = Vec::new();

        for role in main_configuration.roles() {
            let capable_people = main_configuration
                .people()
                .iter()
                .filter(|person| person.has_capability(&role.r#type()))
                .cloned()
                .collect_vec();
            let possible_people = main_configuration
                .people()
                .iter()
                .filter(|person| {
                    person.has_capability(&role.r#type())
                        && !main_configuration
                            .availabilities()
                            .iter()
                            .any(|availability| {
                                availability.person_name() == person.name()
                                    && availability.time().date() == role.time().date()
                            })
                        && !assigmnents
                            .iter()
                            .filter(|assignment| assignment.person() == person.name())
                            .any(|assignment| assignment.role().overlaps(&role))
                })
                .cloned()
                .collect_vec();
            let possible_people_randomized = possible_people
                .choose_multiple(&mut rng, possible_people.len())
                .collect_vec();
            if possible_people_randomized.is_empty() {
                people.push(capable_people[0].name());
            } else {
                let person = possible_people_randomized[0];
                people.push(person.name());
                assigmnents.push(Assignment::new(person.name(), role));
            }
        }

        let roster = Roster::new(assigmnents, main_configuration);
        let roles_for_busiest_person = Self::calculate_roles_for_busiest_person(&roster);
        let constraints_met = Self::calculate_constraints_met(main_configuration, &roster);
        Self {
            people,
            roster,
            roles_for_busiest_person,
            constraints_met,
        }
    }

    pub fn new_randomized_sequential(main_configuration: &MainConfiguration) -> Self {
        let mut rng = thread_rng();

        let mut people = Vec::new();

        let mut assigmnents: Vec<Assignment> = Vec::new();

        let role_types = main_configuration
            .roles()
            .iter()
            .map(|role| role.r#type())
            .unique()
            .collect_vec();

        let role_counts_by_type: HashMap<_, _> = main_configuration
            .roles()
            .iter()
            .into_group_map_by(|role| role.r#type())
            .into_iter()
            .map(|(key, value)| (key, value.len()))
            .collect();

        let people_by_role_type: HashMap<QualifiedId<RoleType>, _> = role_types
            .iter()
            .map(|role_type| {
                (
                    *role_type,
                    main_configuration
                        .people()
                        .into_iter()
                        .filter(|person| person.has_capability(role_type))
                        .collect_vec(),
                )
            })
            .collect();

        let people_by_role_type: HashMap<QualifiedId<RoleType>, _> = people_by_role_type
            .into_iter()
            .map(|(key, value)| {
                (
                    key,
                    value
                        .choose_multiple(&mut rng, value.len())
                        .collect_vec()
                        .into_iter()
                        .cycle()
                        .take(role_counts_by_type[&key])
                        .cloned()
                        .collect_vec(),
                )
            })
            .collect();

        let mut indices: HashMap<_, _> = people_by_role_type.keys().map(|key| (key, 0)).collect();

        for role in main_configuration.roles() {
            let person = people_by_role_type[&role.r#type()][indices[&role.r#type()]].clone();
            *indices.get_mut(&role.r#type()).unwrap() += 1;
            let mut added = false;
            if !main_configuration
                .availabilities()
                .iter()
                .any(|availability| {
                    availability.person_name() == person.name()
                        && availability.time().date() == role.time().date()
                })
                && !assigmnents
                    .iter()
                    .filter(|assignment| assignment.person() == person.name())
                    .any(|assignment| assignment.role().overlaps(&role))
            {
                people.push(person.name());
                assigmnents.push(Assignment::new(person.name(), role));
                added = true;
            } else {
                for person in &people_by_role_type[&role.r#type()] {
                    if !main_configuration
                        .availabilities()
                        .iter()
                        .any(|availability| {
                            availability.person_name() == person.name()
                                && availability.time().date() == role.time().date()
                        })
                        && !assigmnents
                            .iter()
                            .filter(|assignment| assignment.person() == person.name())
                            .any(|assignment| assignment.role().overlaps(&role))
                    {
                        people.push(person.name());
                        assigmnents.push(Assignment::new(person.name(), role.clone()));
                        added = true;
                        break;
                    }
                }
            }

            if !added {
                people.push(person.name());
            }
        }

        let roster = Roster::new(assigmnents, main_configuration);
        let roles_for_busiest_person = Self::calculate_roles_for_busiest_person(&roster);
        let constraints_met = Self::calculate_constraints_met(main_configuration, &roster);
        Self {
            people,
            roster,
            roles_for_busiest_person,
            constraints_met,
        }
    }

    fn new_from_sequence(main_configuration: &MainConfiguration, people: Vec<PersonName>) -> Self {
        let mut assigmnents: Vec<Assignment> = Vec::new();

        for (person, role) in people.iter().zip(main_configuration.roles()) {
            let is_capable = main_configuration
                .person(person)
                .has_capability(&role.r#type())
                && !main_configuration
                    .availabilities()
                    .iter()
                    .any(|availability| {
                        availability.person_name() == person.clone()
                            && availability.time().date() == role.time().date()
                    });
            let is_not_occupied = !assigmnents.iter().any(|assignment| {
                assignment.role().overlaps(&role) && assignment.person() == person.clone()
            });
            if is_capable && is_not_occupied {
                assigmnents.push(Assignment::new(person.clone(), role));
            }
        }

        let roster = Roster::new(assigmnents, main_configuration);
        let roles_for_busiest_person = Self::calculate_roles_for_busiest_person(&roster);
        let constraints_met = Self::calculate_constraints_met(main_configuration, &roster);
        Self {
            people,
            roster,
            roles_for_busiest_person,
            constraints_met,
        }
    }

    fn calculate_constraints_met(main_configuration: &MainConfiguration, roster: &Roster) -> u64 {
        main_configuration
            .constraints()
            .iter()
            .filter(|constraint| !constraint.violated(roster))
            .count() as u64
    }

    pub fn constraints_met(&self) -> u64 {
        self.constraints_met
    }

    pub fn roles_filled(&self) -> u64 {
        self.roster.assignments().len() as u64
    }

    fn calculate_roles_for_busiest_person(roster: &Roster) -> u64 {
        let map = roster
            .assignments()
            .iter()
            .into_group_map_by(|assignment| assignment.person());

        let lengths = map.into_values().map(|value| value.len()).collect_vec();

        let mut output = *lengths.first().unwrap_or(&100000000000000);
        for item in lengths {
            if item > output {
                output = item
            }
        }

        output as u64
    }

    pub fn roles_for_busiest_person(&self) -> u64 {
        self.roles_for_busiest_person
    }

    pub fn roles_locality(&self) {}

    pub fn mutate(&self, main_configuration: &MainConfiguration) -> Self {
        let mut rng = thread_rng();
        let names = main_configuration
            .people()
            .iter()
            .map(|person| person.name().clone())
            .collect_vec();
        let mutated_people = self
            .people
            .iter()
            .map(|person| {
                if rng.gen_bool(0.5) {
                    names.choose(&mut rng).unwrap().clone()
                } else {
                    person.clone()
                }
            })
            .collect();
        Self::new_from_sequence(main_configuration, mutated_people)
    }

    pub fn crossover(&self, main_configuration: &MainConfiguration, other: &Self) -> Self {
        let mut rng = thread_rng();

        if thread_rng().gen_bool(0.5) {
            let crossover_point = rng.gen_range(1..self.people.len());
            let mut front = self.people.clone();
            front[crossover_point] = other.people[crossover_point].clone();
            return Self::new_from_sequence(main_configuration, front);
            /*return Self::new_from_sequence(
            main_configuration,
            self.people
                .iter()
                .zip(other.people.iter())
                .map(|pair| if rng.gen_bool(0.5) { pair.0.clone() } else { pair.1.clone() })
                .collect_vec());*/
        } else {
            let crossover_point = rng.gen_range(1..self.people.len());
            let mut front = self.people.clone();
            for index in crossover_point..rng.gen_range(crossover_point..self.people.len()) {
                front[index] = other.people[index].clone();
            }
            Self::new_from_sequence(main_configuration, front)
        }
    }

    pub fn roster(&self) -> Roster {
        self.roster.clone()
    }
}
