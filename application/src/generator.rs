pub trait Generator<T>
where
    T: Sized,
{
    fn first(&self) -> Option<T>;

    fn generate_next(&self, current: &T) -> Option<T>;
}
