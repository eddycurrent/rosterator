use chrono::{Duration, NaiveDate, Utc};
use eframe::egui::{Button, CentralPanel, ComboBox, Context, ViewportBuilder, ViewportId};
use eframe::epaint::Color32;
use serde::{Deserialize, Serialize};

use crate::availability_definition::AvailabilityDefinition;
use crate::calendar_item_repetition::CalendarItemRepetition;
use crate::person::Person;
use qualified_id::QualifiedId;
use repository::{Repository, SimpleRepository};
use widgets::{DatePicker, TimePicker, TimePickerMode};
use crate::calendar_item_duration::CalendarItemDuration;
use crate::calendar_item_times::CalendarItemTimes;
use crate::colors;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct AddAvailabilityWindowState {
    show: bool,
    person: Option<QualifiedId<Person>>,
    date: NaiveDate,
    start_hour: u8,
    start_minute: u8,
    start_current_selection_mode: TimePickerMode,
    picker_month: NaiveDate,
    end_date: NaiveDate,
    end_hour: u8,
    end_minute: u8,
    end_current_selection_mode: TimePickerMode,
    all_day: bool,
    repeats: CalendarItemRepetition,
    error_message: String,
}

impl AddAvailabilityWindowState {
    pub fn new() -> Self {
        let date = Utc::now().date_naive();
        Self {
            show: false,
            person: None,
            date,
            start_hour: 0,
            start_minute: 0,
            start_current_selection_mode: TimePickerMode::Hour,
            picker_month: Utc::now().date_naive(),
            end_date: date,
            end_hour: 23,
            end_minute: 59,
            end_current_selection_mode: TimePickerMode::Hour,
            all_day: false,
            repeats: CalendarItemRepetition::None,
            error_message: String::new(),
        }
    }

    pub fn reset(&mut self) {
        self.show = false;
        self.person = None;
        self.repeats = CalendarItemRepetition::None;
        self.error_message = String::new();
    }

    pub fn show(&mut self, day: NaiveDate) {
        self.date = day;
        self.end_date = day;
        self.picker_month = day;
        self.show = true;
    }

    pub fn window(
        &mut self,
        ctx: &Context,
        mut add_availability: impl FnMut(AvailabilityDefinition) -> bool,
        people: &SimpleRepository<Person>,
    ) {
        if self.show {
            ctx.show_viewport_immediate(
                ViewportId::from_hash_of("new person window"),
                ViewportBuilder::default().with_inner_size([1000.0, 500.0]),
                |context, _viewport_class| {
                    CentralPanel::default().show(context, |ui| {
                        ui.heading("Person:");
                        ComboBox::from_id_salt("Person:")
                            .selected_text(match self.person {
                                Some(t) => match people.get(&t) {
                                    Some(v) => v.name().value(),
                                    None => String::new(),
                                },
                                None => String::new(),
                            })
                            .show_ui(ui, |ui| {
                                for role in people.iter() {
                                    let label_response = ui.selectable_label(
                                        Some(role.0.clone()) == self.person,
                                        role.1.name().value(),
                                    );
                                    if label_response.clicked() {
                                        self.person = Some(role.0.clone());
                                    }
                                }
                            });
                        
                        ui.checkbox(&mut self.all_day, "All day");

                        ui.add_enabled_ui(!self.all_day, |ui| {
                            ui.horizontal_top(|ui| {
                                ui.heading("Start Time:");
                                TimePicker::new()
                                    .width(150f32)
                                    .hour_hand_color(Color32::from_hex("#E07400").unwrap())
                                    .minute_hand_color(Color32::from_hex("#E07400").unwrap())
                                    .preview_color(Color32::from_hex("#E07400").unwrap())
                                    .show(ui,
                                          &mut self.start_hour,
                                          &mut self.start_minute,
                                          &mut self.start_current_selection_mode);
                                
                                ui.heading("EndDate:");
                                DatePicker::new()
                                    .today_color(Color32::GRAY)
                                    .hover_color(colors::HIGHLIGHT)
                                    .selected_color(colors::HIGHLIGHT)
                                    .show(ui, &mut self.end_date, &mut self.picker_month);

                                ui.heading("End Time:");
                                TimePicker::new()
                                    .width(150f32)
                                    .hour_hand_color(Color32::from_hex("#E07400").unwrap())
                                    .minute_hand_color(Color32::from_hex("#E07400").unwrap())
                                    .preview_color(Color32::from_hex("#E07400").unwrap())
                                    .show(ui,
                                          &mut self.end_hour,
                                          &mut self.end_minute,
                                          &mut self.end_current_selection_mode);
                            });
                        });

                        ui.heading("Repetition:");
                        ComboBox::from_id_salt("Repetition:")
                            .selected_text(match self.repeats {
                                CalendarItemRepetition::None => "None",
                                CalendarItemRepetition::Weekly { .. } => "Weekly",
                            })
                            .show_ui(ui, |ui| {
                                let none_response = ui.selectable_label(
                                    self.repeats == CalendarItemRepetition::None,
                                    "None",
                                );
                                if none_response.clicked() {
                                    self.repeats = CalendarItemRepetition::None;
                                }

                                let weekly_response = ui.selectable_label(
                                    if let CalendarItemRepetition::Weekly { .. } = self.repeats {
                                        true
                                    } else {
                                        false
                                    },
                                    "Weekly",
                                );
                                if weekly_response.clicked() {
                                    self.repeats = CalendarItemRepetition::Weekly { count: None };
                                }
                            });

                        ui.label(self.error_message.clone());

                        ui.horizontal(|ui| {
                            let add_button = Button::new("Add");
                            let add_button_response = ui.add(add_button);
                            if add_button_response.clicked() {
                                let start_date_time = if self.all_day {
                                    self.date.and_hms_opt(0, 0, 0).unwrap()
                                } else {
                                    self.date
                                        .and_hms_opt(
                                            self.start_hour as u32,
                                            self.start_minute as u32,
                                            0)
                                        .unwrap()
                                };
                                let end_date_time = if self.all_day {
                                    self.end_date.and_hms_opt(23, 59, 0).unwrap()
                                } else {
                                    self.end_date
                                        .and_hms_opt(
                                            self.end_hour as u32,
                                            self.end_minute as u32,
                                            0)
                                        .unwrap()
                                };
                                let role = AvailabilityDefinition::new(
                                    // TODO remove unwraps
                                    people.get(&self.person.unwrap()).unwrap().name(),
                                    // TODO remove unwrap
                                    self.person.unwrap().clone(),
                                    // TODO remove unwrap
                                    CalendarItemTimes::new(start_date_time, self.repeats),
                                    CalendarItemDuration::new(Duration::from(end_date_time - start_date_time)),
                                );
                                let added_role = add_availability(role);
                                if added_role {
                                    self.reset();
                                } else {
                                    self.error_message = "role already exists".into();
                                }
                            }

                            let cancel_button = Button::new("Cancel");
                            let cancel_button_response = ui.add(cancel_button);
                            if cancel_button_response.clicked() {
                                self.reset();
                            }
                        });
                    });
                    if context.input(|input_state| input_state.viewport().close_requested()) {
                        self.reset();
                    }
                },
            );
        }
    }
}
