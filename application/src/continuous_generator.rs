use crate::generator::Generator;
use crate::generator_predicate::GeneratorStopPredicate;

pub trait ContinuousGenerator<TGenerated, StopState>
where
    TGenerated: GeneratorStopPredicate<StopState>,
    StopState: PartialOrd,
{
    fn generate_until(&self, stop_state: &StopState) -> Vec<TGenerated>;
}

impl<TSource, TGenerated, StopState> ContinuousGenerator<TGenerated, StopState> for TSource
where
    TSource: Generator<TGenerated>,
    TGenerated: Clone + GeneratorStopPredicate<StopState>,
    StopState: PartialOrd,
{
    fn generate_until(&self, stop_state: &StopState) -> Vec<TGenerated> {
        let mut output = Vec::new();
        let mut next = self.first();
        while let Some(value) = next
            && !value.stop(stop_state)
        {
            output.push(value.clone());
            next = self.generate_next(&value);
        }
        output
    }
}
