use eframe::epaint::Color32;

pub static HIGHLIGHT: Color32 = Color32::from_rgb(224, 116, 0);