use crate::add_availability_window_state::AddAvailabilityWindowState;
use crate::add_role_window_state::AddRoleWindowState;
use crate::algorithm_progress::AlgorithmProgress;
use crate::availability::Availability;
use crate::constraint::Constraint;
use crate::continuous_generator::ContinuousGenerator;
use crate::main_configuration::MainConfiguration;
use crate::people_page_view_model::PeoplePageViewModel;
use crate::project::Project;
use crate::role::Role;
use crate::role_type_page_view_model::RoleTypePageViewModel;
use crate::roster::Roster;
use crate::{colors, plus_button, run_ga, Page};
use chrono::Datelike;
use chrono::{Days, Month, Months, NaiveDate, Utc};
use eframe::emath::{vec2, Align, Vec2b};
use eframe::epaint::{Color32, Margin, Rounding, Stroke};
use egui::{
    Button, CentralPanel, ComboBox, Context, Frame, Grid, Key, KeyboardShortcut, Label, Layout,
    Modifiers, ProgressBar, ScrollArea, Sense, TextStyle, Ui, Widget,
};
use egui_commonmark::CommonMarkCache;
use egui_extras::DatePickerButton;
use egui_file_dialog::FileDialog;
use egui_plot::{Line, Plot, PlotPoints};
use itertools::Itertools;
use repository::Repository;
use std::collections::HashMap;
use std::fs::{read_to_string, write};
use std::ops::{Add, Sub};
use std::sync::mpsc::{channel, Receiver};
use std::thread;
use widgets::task_view::monthly::{MonthRosterWidget, TaskViewMonth};

pub struct ApplicationState {
    page: Page,
    role_type_page_view_model: RoleTypePageViewModel,
    current_month: NaiveDate,
    add_role_window_state: AddRoleWindowState,
    people_page_view_model: PeoplePageViewModel,
    add_availability_window_state: AddAvailabilityWindowState,
    availability_view: MonthRosterWidget,
    errors: Vec<String>,
    saving: bool,
    file_dialog: FileDialog,
    setup: Option<Project>,
    roster: Option<Roster>,
    roster_thread: Option<Receiver<Roster>>,
    roster_progress_receiver: Option<Receiver<AlgorithmProgress>>,
    roster_progress: f32,
    roles_filled_by_generation: Vec<u64>,
    zero_separated_by_generation: Vec<u64>,
    one_separated_by_generation: Vec<u64>,
    two_separated_by_generation: Vec<u64>,
    three_separated_by_generation: Vec<u64>,
    four_separated_by_generation: Vec<u64>,
    five_separated_by_generation: Vec<u64>,
    selected_roster: String,
    cache: CommonMarkCache,
}

impl Default for ApplicationState {
    fn default() -> Self {
        Self {
            page: Page::RoleTypes,
            role_type_page_view_model: RoleTypePageViewModel::new(),
            current_month: Utc::now().date_naive(),
            add_role_window_state: AddRoleWindowState::new(),
            people_page_view_model: PeoplePageViewModel::new(),
            add_availability_window_state: AddAvailabilityWindowState::new(),
            availability_view: MonthRosterWidget::new(),
            errors: Vec::new(),
            saving: false,
            file_dialog: FileDialog::new(),
            setup: None,
            roster: None,
            roster_thread: None,
            roster_progress_receiver: None,
            roster_progress: 0.0,
            roles_filled_by_generation: Vec::new(),
            zero_separated_by_generation: Vec::new(),
            one_separated_by_generation: Vec::new(),
            two_separated_by_generation: Vec::new(),
            three_separated_by_generation: Vec::new(),
            four_separated_by_generation: Vec::new(),
            five_separated_by_generation: Vec::new(),
            selected_roster: "current".to_string(),
            cache: CommonMarkCache::default(),
        }
    }
}
impl ApplicationState {
    /// Called once before the first frame.
    pub fn new(_cc: &eframe::CreationContext<'_>) -> Self {
        // This is also where you can customize the look and feel of egui using
        // `cc.egui_ctx.set_visuals` and `cc.egui_ctx.set_fonts`.

        Default::default()
    }

    fn tab_button(&mut self, ui: &mut Ui, target_page: Page) {
        let role_types_button = Button::new(target_page.to_title_string())
            .stroke(Stroke::default())
            .min_size(vec2(150f32, 40f32))
            .selected(self.page == target_page);
        let role_types_button_response = ui.add(role_types_button);
        if role_types_button_response.clicked() {
            self.page = target_page;
        }
    }

    pub fn roles_page(&mut self, ui: &mut Ui, setup: &mut Project) {
        ui.with_layout(Layout::right_to_left(Align::TOP), |ui| {
            ui.hyperlink_to(
                "Need help?",
                "https://gitlab.com/eddycurrent/rosterator/-/wikis/concepts/role",
            );
        });

        let ui_height = ui.available_height();
        ui.horizontal(|ui| {
            ui.with_layout(Layout::left_to_right(Align::TOP), |ui| {
                ui.vertical(|ui| {
                    self.calendar_headers(ui);

                    ui.spacing_mut().item_spacing = vec2(0.0, 5.0);

                    let days = self.calculate_visible_month_days();

                    let roles = setup
                        .role_definitions
                        .iter()
                        .flat_map(|definition| definition.generate_until(days.last().unwrap()))
                        .collect_vec();

                    let column_width = ui.available_size().x / 7.0;
                    let number_of_weeks = (days.len() / 7) as f32;
                    let column_height =
                        (ui_height - 40.0 - number_of_weeks * 5.0) / number_of_weeks;
                    Grid::new("calendar grid")
                        .min_col_width(column_width)
                        .max_col_width(column_width)
                        .min_row_height(column_height)
                        .show(ui, |ui| {
                            for day in days.into_iter().enumerate() {
                                self.role_calendar_day(
                                    ui,
                                    column_height,
                                    day.0,
                                    day.1,
                                    &roles,
                                    setup,
                                );
                                if (day.0 + 1) % 7 == 0 && day.0 > 0 {
                                    ui.end_row();
                                }
                            }
                        });
                });
            });
        });
    }

    pub fn availability_page(&mut self, ui: &mut Ui, setup: &mut Project) {
        ui.with_layout(Layout::right_to_left(Align::TOP), |ui| {
            ui.hyperlink_to(
                "Need help?",
                "https://gitlab.com/eddycurrent/rosterator/-/wikis/concepts/availability",
            );
        });

        let ui_height = ui.available_height();
        ui.horizontal(|ui| {
            ui.with_layout(Layout::left_to_right(Align::TOP), |ui| {
                ui.vertical(|ui| {
                    let days = self.calculate_visible_month_days();

                    let availabilities = setup
                        .availability_definitions
                        .iter()
                        .flat_map(|definition| definition.generate_until(days.last().unwrap()))
                        .enumerate()
                        .map(|availability| TaskViewMonth {
                            id: availability.0 as u32,
                            title: format!(
                                "{} {}-{}",
                                availability.1.person_name().value(),
                                availability.1.time(),
                                availability.1.time() + availability.1.duration().duration()),
                            start_date: availability.1.time().date(),
                            end_date: (availability.1.time() + availability.1.duration().duration()).date(),
                            color: colors::HIGHLIGHT })
                        .collect_vec();

                    self.availability_view.set_width(ui.available_width());
                    self.availability_view.set_height(ui_height);

                    self.availability_view
                        .show(ui, &availabilities);
                    if let Some(date) = self.availability_view.clicked_date() {
                        self.add_availability_window_state.show(date);
                    }
                });
            });
        });
    }

    fn calendar_headers(&mut self, ui: &mut Ui) {
        ui.spacing_mut().item_spacing = vec2(0.0, 5.0);
        self.calendar_control_header(ui);

        ui.spacing_mut().item_spacing = vec2(0.0, 0.0);
        Self::calndar_day_row(ui);
    }

    fn calendar_control_header(&mut self, ui: &mut Ui) {
        ui.columns(3, |uis| {
            uis[0].with_layout(Layout::top_down(Align::Center), |ui| {
                ui.set_min_height(20.0);
                let button = Button::new("◀").min_size(ui.available_size());
                let back_button_response = ui.add(button);
                if back_button_response.clicked() {
                    self.current_month = self.current_month.sub(Months::new(1));
                }
            });
            uis[1].with_layout(Layout::top_down(Align::Center), |ui| {
                ui.set_min_height(20.0);
                ui.label(format!(
                    "{} {}",
                    Month::try_from(self.current_month.month() as u8)
                        .unwrap()
                        .name(),
                    self.current_month.year()
                ));
            });
            uis[2].with_layout(Layout::top_down(Align::Center), |ui| {
                ui.set_min_height(20.0);
                let button = Button::new("▶").min_size(ui.available_size());
                let forward_button_response = ui.add(button);
                if forward_button_response.clicked() {
                    self.current_month = self.current_month.add(Months::new(1));
                }
            });
        });
    }

    fn calndar_day_row(ui: &mut Ui) {
        ui.columns(7, |uis| {
            uis[0].with_layout(Layout::top_down(Align::Center), |ui| {
                ui.set_min_height(20.0);
                ui.label("Sun");
            });
            uis[1].with_layout(Layout::top_down(Align::Center), |ui| {
                ui.set_min_height(20.0);
                ui.label("Mon");
            });
            uis[2].with_layout(Layout::top_down(Align::Center), |ui| {
                ui.set_min_height(20.0);
                ui.label("Tue");
            });
            uis[3].with_layout(Layout::top_down(Align::Center), |ui| {
                ui.set_min_height(20.0);
                ui.label("Wed");
            });
            uis[4].with_layout(Layout::top_down(Align::Center), |ui| {
                ui.set_min_height(20.0);
                ui.label("Thu");
            });
            uis[5].with_layout(Layout::top_down(Align::Center), |ui| {
                ui.set_min_height(20.0);
                ui.label("Fri");
            });
            uis[6].with_layout(Layout::top_down(Align::Center), |ui| {
                ui.set_min_height(20.0);
                ui.label("Sat");
            });
        });
    }

    fn calculate_visible_month_days(&self) -> Vec<NaiveDate> {
        let month_start = self.current_month.with_day0(0).unwrap();
        let start_day = month_start.weekday();

        let month_length = month_start.add(Months::new(1)) - month_start;

        let mut days = (0..start_day.num_days_from_sunday())
            .rev()
            .map(|days| month_start.sub(Days::new(days as u64 + 1)))
            .collect_vec();

        for day in 0..month_length.num_days() {
            days.push(month_start.add(Days::new(day as u64)));
        }

        while days.len() % 7 != 0 {
            days.push(days.last().unwrap().add(Days::new(1)));
        }

        days
    }

    fn role_calendar_day(
        &mut self,
        ui: &mut Ui,
        height: f32,
        grid_index: usize,
        date: NaiveDate,
        roles: &Vec<Role>,
        setup: &mut Project,
    ) {
        let inactive = self.current_month.month() != date.month();
        Frame::none()
            .stroke(Stroke::new(1.0, Color32::DARK_GRAY))
            .show(ui, |ui| {
                ui.vertical(|ui| {
                    ui.spacing_mut().item_spacing = vec2(0.0, 0.0);
                    ui.with_layout(Layout::top_down_justified(Align::Center), |ui| {
                        ui.label(date.day().to_string());
                    });
                    ui.separator();
                    let role_width = ui.available_width();

                    ui.push_id(format!("{}", grid_index), |ui| {
                        ScrollArea::vertical()
                            .max_height(height - 5.0)
                            .show(ui, |ui| {
                                Grid::new(format!("{}_roles", date))
                                    .spacing(vec2(0.0, 5.0))
                                    .min_col_width(role_width)
                                    .max_col_width(role_width)
                                    .show(ui, |ui| {
                                        let roles_for_day =
                                            roles.iter().filter(|role| role.time().date() == date);
                                        for role in roles_for_day {
                                            Self::role_calendar_item(
                                                ui,
                                                if inactive {
                                                    Color32::GRAY
                                                } else {
                                                    Color32::from_hex("#E07400").unwrap()
                                                },
                                                date,
                                                role,
                                                setup,
                                            );
                                            ui.end_row();
                                        }
                                    });
                            });
                    });

                    plus_button(ui, || self.add_role_window_state.show(date));
                });
            });
    }

    fn role_calendar_item(
        ui: &mut Ui,
        background_color: Color32,
        day: NaiveDate,
        role: &Role,
        setup: &mut Project,
    ) {
        let frame_response = Frame::none()
            .fill(background_color)
            .outer_margin(Margin::symmetric(5.0, 0.0))
            .inner_margin(Margin::symmetric(5.0, 0.0))
            .rounding(Rounding::same(5.0))
            .show(ui, |ui| {
                ui.set_min_width(ui.available_width());
                ui.style_mut().visuals.override_text_color = Some(Color32::BLACK);
                let label = Label::new(role.title()).selectable(false);
                ui.add(label);
            });

        frame_response.response.context_menu(|ui| {
            Self::role_calendar_item_context_menu_item(ui, "delete", || {
                setup.role_definitions = setup
                    .role_definitions
                    .iter()
                    .cloned()
                    .filter_map(|r| {
                        if r.title() == role.title() {
                            r.remove_time(day.and_time(role.time().time()))
                        } else {
                            Some(r)
                        }
                    })
                    .collect();
            });
            Self::role_calendar_item_context_menu_item(ui, "delete all", || {
                setup.role_definitions = setup
                    .role_definitions
                    .iter()
                    .cloned()
                    .filter_map(|r| {
                        if r.title() == role.title() {
                            None
                        } else {
                            Some(r)
                        }
                    })
                    .collect();
            });
        });
    }

    fn availability_calendar_item(
        ui: &mut Ui,
        background_color: Color32,
        day: NaiveDate,
        availability: &Availability,
        setup: &mut Project,
    ) {
        // TODO: remove this unwrap
        let person = setup.people.get(&availability.person()).unwrap();

        let frame_response = Frame::none()
            .fill(background_color)
            .outer_margin(Margin::symmetric(5.0, 0.0))
            .inner_margin(Margin::symmetric(5.0, 0.0))
            .rounding(Rounding::same(5.0))
            .show(ui, |ui| {
                ui.set_min_width(ui.available_width());
                ui.style_mut().visuals.override_text_color = Some(Color32::BLACK);
                let label = Label::new(person.name()).selectable(false);
                ui.add(label);
            });

        frame_response.response.context_menu(|ui| {
            Self::role_calendar_item_context_menu_item(ui, "delete", || {
                setup.availability_definitions = setup
                    .availability_definitions
                    .iter()
                    .cloned()
                    .filter_map(|r| {
                        if r.person() == availability.person() {
                            r.remove_time(day.and_time(availability.time().time()))
                        } else {
                            Some(r)
                        }
                    })
                    .collect();
            });
            Self::role_calendar_item_context_menu_item(ui, "delete all", || {
                setup.availability_definitions = setup
                    .availability_definitions
                    .iter()
                    .cloned()
                    .filter_map(|r| {
                        if r.person() == availability.person() {
                            None
                        } else {
                            Some(r)
                        }
                    })
                    .collect();
            });
        });
    }

    fn role_calendar_item_context_menu_item(
        ui: &mut Ui,
        title: impl Into<String>,
        on_clicked: impl FnOnce(),
    ) {
        let label = Label::new(title.into())
            .selectable(false)
            .sense(Sense::click());

        let label_response = ui.add(label);
        if label_response.clicked() {
            on_clicked();
        }
    }

    pub fn constraints_page(&mut self, ui: &mut Ui) {
        ui.with_layout(Layout::right_to_left(Align::TOP), |ui| {
            ui.hyperlink_to(
                "Need help?",
                "https://gitlab.com/eddycurrent/rosterator/-/wikis/concepts/constraint",
            );
        });

        ui.label("This page has no contents yet, still under development.");
    }
    pub fn algorithm_parameters_page(&mut self, ui: &mut Ui, setup: &mut Project) {
        ui.with_layout(Layout::right_to_left(Align::TOP), |ui| {
            ui.hyperlink_to(
                "Need help?",
                "https://gitlab.com/eddycurrent/rosterator/-/wikis/algorithm-parameters",
            );
        });

        ui.heading("Schedule From:");
        let mut new_from_date = setup.schedule_from.date();
        DatePickerButton::new(&mut new_from_date)
            .id_salt("Schedule From:")
            .ui(ui);
        setup.schedule_from = new_from_date.and_time(setup.schedule_to.time());

        ui.heading("Schedule To:");
        let mut new_to_date = setup.schedule_to.date();
        DatePickerButton::new(&mut new_to_date)
            .id_salt("Schedule To")
            .ui(ui);
        setup.schedule_to = new_to_date.and_time(setup.schedule_to.time());
    }

    pub fn run_page(&mut self, ui: &mut Ui, setup: &mut Project) {
        ui.with_layout(Layout::right_to_left(Align::TOP), |ui| {
            ui.hyperlink_to(
                "Need help?",
                "https://gitlab.com/eddycurrent/rosterator/-/wikis/run",
            );
        });

        let button = Button::new("Run")
            .min_size(vec2(30.0, 30.0))
            .rounding(Rounding::same(15.0));
        let person_add_button_response = ui.add(button);
        if person_add_button_response.clicked() {
            // TODO: configure range
            let roles = setup
                .role_definitions
                .iter()
                .flat_map(|definition| definition.generate_until(&setup.schedule_to.date()))
                .filter(|role| role.time() > setup.schedule_from)
                .collect_vec();
            let availabilities = setup
                .availability_definitions
                .iter()
                .flat_map(|definition| definition.generate_until(&setup.schedule_to.date()))
                .filter(|role| role.time() > setup.schedule_from)
                .collect_vec();

            let main_configuration = MainConfiguration::new(
                1000,
                Vec::<Constraint>::new(),
                roles,
                setup.people.values(),
                availabilities,
            );
            match main_configuration {
                // :TODO: handle unwraps
                Ok(main_configuration) => {
                    let (roster_sender, roster_receiver) = channel();
                    self.roster_thread = Some(roster_receiver);
                    let (progress_sender, progress_receiver) = channel();
                    self.roster_progress_receiver = Some(progress_receiver);
                    self.errors.clear();
                    thread::spawn(move || {
                        let roster = run_ga(&main_configuration, |progress| {
                            progress_sender.send(progress).unwrap()
                        });
                        roster_sender.send(roster).unwrap();
                    });
                }
                Err(errors) => self.errors = errors,
            }
        }
        for error in &self.errors {
            ui.label(error);
        }

        if let Some(progress_receiver) = &self.roster_progress_receiver {
            if let Ok(progress) = progress_receiver.try_recv() {
                let distribution_status = progress.distribution_status();
                self.roster_progress = progress.generations_done_percent();
                self.roles_filled_by_generation
                    .push(progress.roles_filled());
                self.zero_separated_by_generation
                    .push(distribution_status[0]);
                self.one_separated_by_generation
                    .push(distribution_status[1]);
                self.two_separated_by_generation
                    .push(distribution_status[2]);
                self.three_separated_by_generation
                    .push(distribution_status[3]);
                self.four_separated_by_generation
                    .push(distribution_status[4]);
                self.five_separated_by_generation
                    .push(distribution_status[5]);
            }

            ProgressBar::new(self.roster_progress).animate(true).ui(ui);

            ScrollArea::vertical().show(ui, |ui| {
                Self::progress_graph(
                    ui,
                    "roles filled",
                    "roles filled",
                    self.roles_filled_by_generation
                        .iter()
                        .map(|value| *value as f64),
                );
                Self::progress_graph(
                    ui,
                    "0 in a row",
                    "# people separated by 0 tasks",
                    self.zero_separated_by_generation
                        .iter()
                        .map(|value| *value as f64),
                );
                Self::progress_graph(
                    ui,
                    "1 in a row",
                    "# people separated by 1 tasks",
                    self.one_separated_by_generation
                        .iter()
                        .map(|value| *value as f64),
                );
                Self::progress_graph(
                    ui,
                    "2 in a row",
                    "# people separated by 2 tasks",
                    self.two_separated_by_generation
                        .iter()
                        .map(|value| *value as f64),
                );
                Self::progress_graph(
                    ui,
                    "3 in a row",
                    "# people separated by 3 tasks",
                    self.three_separated_by_generation
                        .iter()
                        .map(|value| *value as f64),
                );
                Self::progress_graph(
                    ui,
                    "4 in a row",
                    "# people separated by 4 tasks",
                    self.four_separated_by_generation
                        .iter()
                        .map(|value| *value as f64),
                );
                Self::progress_graph(
                    ui,
                    "5 in a row",
                    "# people separated by 5 tasks",
                    self.five_separated_by_generation
                        .iter()
                        .map(|value| *value as f64),
                );
            });
        }
    }

    fn progress_graph<TPoints>(ui: &mut Ui, id: &str, y_axis_label: &str, points: TPoints)
    where
        TPoints: Iterator<Item = f64>,
    {
        let points: PlotPoints = points
            .enumerate()
            .map(|(generation, value)| [generation as f64, value])
            .collect();
        let line = Line::new(points);
        Plot::new(id)
            .allow_zoom(false)
            .allow_scroll(false)
            .allow_drag(false)
            .allow_boxed_zoom(false)
            .auto_bounds(Vec2b::new(false, true))
            .include_x(0)
            .include_x(1000)
            .set_margin_fraction(vec2(0f32, 0f32))
            .clamp_grid(true)
            .width(ui.available_width())
            .height(200f32)
            .y_axis_label(y_axis_label)
            .show(ui, |plot_ui| plot_ui.line(line));
    }

    pub fn view_page(&mut self, ui: &mut Ui, project: &mut Project) {
        ui.with_layout(Layout::right_to_left(Align::TOP), |ui| {
            ui.hyperlink_to(
                "Need help?",
                "https://gitlab.com/eddycurrent/rosterator/-/wikis/view",
            );
        });

        ComboBox::new("Select Roster", "Shown roster")
            .selected_text(&self.selected_roster)
            .show_ui(ui, |ui| {
                if ui
                    .selectable_label(self.selected_roster == "current", "current")
                    .clicked()
                {
                    self.selected_roster = "current".to_string();
                }

                for roster in project.saved_rosters.keys() {
                    if ui
                        .selectable_label(&self.selected_roster == roster, roster)
                        .clicked()
                    {
                        self.selected_roster = roster.to_string();
                    }
                }
            });

        let roster = if self.selected_roster == "current" {
            &self.roster
        } else {
            &project.saved_rosters.get(&self.selected_roster).cloned()
        };

        if let Some(roster) = roster {
            if self.selected_roster == "current" {
                if ui.button("Save roster").clicked() {
                    project
                        .saved_rosters
                        .insert(Utc::now().to_string(), roster.clone());
                }
            }

            let roles = roster
                .assignments()
                .iter()
                // TODO: remove unwrap
                .map(|assignment| project.role_types.get(&assignment.role().r#type()).unwrap())
                .unique()
                .sorted()
                .collect_vec();

            let mut assignments_by_day_by_role_title = HashMap::new();
            for assignment in roster.assignments() {
                let date = assignment.role().time().date();
                // TODO: remove unwrap
                let type_name = project
                    .role_types
                    .get(&assignment.role().r#type())
                    .unwrap()
                    .value();
                assignments_by_day_by_role_title
                    .entry(date)
                    .or_insert_with(HashMap::new);
                let inner = assignments_by_day_by_role_title.get_mut(&date).unwrap();
                inner
                    .entry(type_name)
                    .or_insert_with(Vec::new)
                    .push(assignment);
            }

            let days = roster
                .assignments()
                .iter()
                .map(|assignment| assignment.role().time().date())
                .unique()
                .collect_vec();
            ScrollArea::vertical().show(ui, |ui| {
                Grid::new("View Roster Grid").show(ui, |ui| {
                    ui.label(String::new());
                    for role in &roles {
                        ui.label(role.value().clone());
                    }
                    ui.end_row();

                    for day in &days {
                        ui.label(day.to_string());
                        for role in &roles {
                            let assignment = assignments_by_day_by_role_title
                                .get(day)
                                .and_then(|assignments_by_role| {
                                    assignments_by_role.get(&role.value()).map(|a| {
                                        a.iter()
                                            .map(|assignment| assignment.person().value())
                                            .join("\n")
                                    })
                                })
                                .unwrap_or(String::new());
                            ui.label(assignment);
                        }
                        ui.end_row();
                    }
                });
            });
        }
    }
}

impl eframe::App for ApplicationState {
    fn update(&mut self, ctx: &Context, _frame: &mut eframe::Frame) {
        self.setup = match &self.setup {
            Some(setup) => {
                let mut setup = setup.clone();

                if let Some(path) = self.file_dialog.update(ctx).picked()
                    && self.saving
                {
                    let path_string = path.to_str().unwrap().to_string();
                    setup.path = Some(path_string);
                    let serialized_content = ron::to_string(&setup).unwrap();
                    write(path, serialized_content).unwrap();
                    self.saving = false;
                }

                ctx.input_mut(|input| {
                    if input.consume_shortcut(&KeyboardShortcut::new(Modifiers::CTRL, Key::S)) {
                        match &setup.path {
                            Some(path) => {
                                let serialized_content = ron::to_string(&setup).unwrap();
                                write(path, serialized_content).unwrap()
                            }
                            None => {
                                self.file_dialog.save_file();
                                self.saving = true;
                            }
                        }
                    }
                });

                if let Some(receiver) = &self.roster_thread {
                    if let Ok(roster) = receiver.try_recv() {
                        self.roster = Some(roster);
                        self.roster_thread = None;
                        self.roster_progress_receiver = None;
                        self.roster_progress = 0.0;
                    }
                }

                egui::SidePanel::left("view selector panel").show(ctx, |ui| {
                    ui.vertical(|ui| {
                        ui.text_style_height(&TextStyle::Heading);

                        self.tab_button(ui, Page::RoleTypes);
                        self.tab_button(ui, Page::Roles);
                        self.tab_button(ui, Page::People);
                        self.tab_button(ui, Page::Availability);
                        self.tab_button(ui, Page::Constraints);
                        self.tab_button(ui, Page::AlgorithmParameters);
                        self.tab_button(ui, Page::Run);
                        self.tab_button(ui, Page::View);
                    });
                });

                CentralPanel::default().show(ctx, |ui| {
                    ui.style_mut().url_in_tooltip = true;

                    match self.page {
                        Page::RoleTypes => self.role_type_page_view_model.render(
                            ui,
                            &mut setup.role_types,
                            &mut self.cache,
                        ),
                        Page::Roles => self.roles_page(ui, &mut setup),
                        Page::People => self.people_page_view_model.render(
                            ui,
                            &mut setup.people,
                            &setup.role_types,
                        ),
                        Page::Availability => self.availability_page(ui, &mut setup),
                        Page::Constraints => self.constraints_page(ui),
                        Page::AlgorithmParameters => self.algorithm_parameters_page(ui, &mut setup),
                        Page::Run => self.run_page(ui, &mut setup),
                        Page::View => self.view_page(ui, &mut setup),
                    }
                });
                self.add_role_window_state.window(
                    ctx,
                    |role| setup.role_definitions.insert(role),
                    &setup.role_types,
                );
                self.add_availability_window_state.window(
                    ctx,
                    |role| setup.availability_definitions.insert(role),
                    &setup.people,
                );
                Some(setup)
            }
            None => {
                let mut output = None;
                CentralPanel::default().show(ctx, |ui| {
                    Grid::new("Welcome Screen")
                        .num_columns(1)
                        .min_row_height(ui.available_height() / 2f32)
                        .show(ui, |ui| {
                            ui.with_layout(Layout::bottom_up(Align::Center), |ui| {
                                let open_button = Button::new("Open").min_size(vec2(200f32, 40f32));
                                let open_button_response = ui.add(open_button);
                                if open_button_response.clicked() {
                                    self.file_dialog.pick_file();
                                }

                                if let Some(path) = self.file_dialog.update(ctx).picked() {
                                    match read_to_string(path) {
                                        Ok(content) => match ron::from_str(&content) {
                                            Ok(setup) => output = Some(setup),
                                            Err(error) => {
                                                println!("error deserializing setup: {}", error)
                                            }
                                        },
                                        Err(error) => println!("error opening file: {}", error),
                                    }
                                }
                            });
                            ui.end_row();
                            ui.with_layout(Layout::top_down(Align::Center), |ui| {
                                let new_button = Button::new("New").min_size(vec2(200f32, 40f32));
                                let new_button_response = ui.add(new_button);
                                if new_button_response.clicked() {
                                    output = Some(Project::empty());
                                }
                            });
                        });
                });
                output
            }
        }
    }
}
