use eframe::epaint::Color32;
use egui::{Button, Frame, Label, ScrollArea, TextEdit, Widget};
use egui_commonmark::{CommonMarkCache, CommonMarkViewer};
use egui_extras::{TableBody, TableRow};
use itertools::Itertools;
use serde::{Deserialize, Serialize};

use qualified_id::QualifiedId;
use repository::{Repository, SimpleRepository};
use crate::role::RoleType;

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct RoleTypeRowViewModel {
    role_type_id: QualifiedId<RoleType>,
    type_edit: Option<String>,
    description_edit: Option<String>,
    description_expanded: bool,
    height: f32,
}

impl RoleTypeRowViewModel {
    pub fn new(role_type: QualifiedId<RoleType>) -> Self {
        Self {
            role_type_id: role_type,
            type_edit: None,
            description_edit: None,
            description_expanded: false,
            height: 50.0,
        }
    }

    pub fn with_edits(
        &self,
        type_edit: Option<String>,
        description_state: (Option<String>, bool, f32),
    ) -> Self {
        Self {
            role_type_id: self.role_type_id,
            type_edit,
            description_edit: description_state.0,
            description_expanded: description_state.1,
            height: description_state.2,
        }
    }

    pub fn role_type_id(&self) -> QualifiedId<RoleType> {
        self.role_type_id
    }

    fn render_type(&self, row: &mut TableRow, role_type: &mut RoleType) -> Option<String> {
        let mut output = self.type_edit.clone();
        row.col(|ui| match &self.type_edit {
            Some(value) => {
                let mut value = value.clone();
                ui.text_edit_singleline(&mut value);
                ui.horizontal(|ui| {
                    let save_button = Button::new("Save");
                    let save_button_response = ui.add(save_button);
                    let cancel_button = Button::new("Cancel");
                    let cancel_button_response = ui.add(cancel_button);
                    if save_button_response.clicked() {
                        *role_type = role_type.with_type(value);
                        output = None;
                    } else if cancel_button_response.clicked() {
                        output = None;
                    } else {
                        output = Some(value);
                    }
                });
            }
            None => {
                let person_name_label = Label::new(role_type.value());
                let person_name_label_response = ui.add(person_name_label);
                if person_name_label_response.clicked() {
                    output = Some(role_type.value());
                }
            }
        });
        output
    }

    fn render_description(
        &self,
        row: &mut TableRow,
        role_type: &mut RoleType,
        cache: &mut CommonMarkCache,
    ) -> (Option<String>, bool, f32) {
        let mut output = self.description_edit.clone();
        let mut expanded = false;
        let mut height = self.height;
        row.col(|ui| {
            let text_width = ui.available_width();
            match &self.description_edit {
                Some(value) => {
                    let mut value = value.clone();
                    ScrollArea::vertical()
                        .min_scrolled_height(ui.available_height())
                        .show(ui, |ui| {
                            TextEdit::multiline(&mut value)
                                .desired_width(text_width)
                                .ui(ui);
                        });
                    ui.horizontal(|ui| {
                        let save_button = Button::new("Save");
                        let save_button_response = ui.add(save_button);
                        let cancel_button = Button::new("Cancel");
                        let cancel_button_response = ui.add(cancel_button);
                        if save_button_response.clicked() {
                            *role_type = role_type.with_description(value);
                            output = None;
                        } else if cancel_button_response.clicked() {
                            output = None;
                        } else {
                            output = Some(value)
                        }
                    });
                }
                None => {
                    if Frame::none()
                        .show(ui, |ui| {
                            let description = role_type.description();
                            let description_lines = description.split("\n").collect_vec();
                            let markdown_response = CommonMarkViewer::new()
                                .show(
                                    ui,
                                    cache,
                                    &(description_lines
                                        .iter()
                                        .take(if self.description_expanded {
                                            description_lines.len()
                                        } else {
                                            1
                                        })
                                        .join("\n")
                                        + if !self.description_expanded
                                            && description_lines.len() > 1
                                        {
                                            "..."
                                        } else {
                                            ""
                                        }),
                                )
                                .response;
                            height = markdown_response
                                .intrinsic_size
                                .unwrap_or(markdown_response.rect.size())
                                .y
                                + 20.0;
                            height = height.max(20.0);
                            if ui.button("edit").clicked() {
                                output = Some(role_type.description());
                            }
                        })
                        .response
                        .hovered()
                    {
                        expanded = true;
                    }
                }
            }
        });
        (output, expanded, height)
    }

    pub fn render(
        &mut self,
        table_body: &mut TableBody,
        role_types: &mut SimpleRepository<RoleType>,
        cache: &mut CommonMarkCache,
    ) -> Option<Self> {
        role_types
            .get_mut(&self.role_type_id)
            .and_then(|role_type| {
                let row_height = if self.description_edit.is_some() {
                    100.0
                } else {
                    self.height
                };
                let mut output = None;
                table_body.row(row_height, |mut row| {
                    output = Some(self.with_edits(
                        self.render_type(&mut row, role_type),
                        self.render_description(&mut row, role_type, cache),
                    ));
                    row.col(|ui| {
                        let delete_button = Button::new("Delete").fill(Color32::DARK_RED);
                        let delete_button_response = ui.add(delete_button);
                        if delete_button_response.clicked() {
                            output = None;
                        }
                    });
                });
                output
            })
    }
}
