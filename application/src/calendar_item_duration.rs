use chrono::Duration;
use serde::{Deserialize, Deserializer, Serialize, Serializer};

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub struct CalendarItemDuration {
    duration: Duration,
}

impl CalendarItemDuration {
    pub fn new(duration: Duration) -> Self {
        Self { duration }
    }

    pub fn duration(&self) -> Duration {
        self.duration
    }
}

impl Serialize for CalendarItemDuration {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_i64(self.duration.num_nanoseconds().unwrap())
    }
}

impl<'de> Deserialize<'de> for CalendarItemDuration {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        Ok(Self::new(Duration::nanoseconds(i64::deserialize(
            deserializer,
        )?)))
    }
}
