use serde::{Deserialize, Serialize};
use std::hash::Hash;

#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum CalendarItemRepetition {
    None,
    Weekly { count: Option<usize> },
}
