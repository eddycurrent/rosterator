use chrono::{Duration, NaiveDate, Utc};
use eframe::egui::{Button, CentralPanel, ComboBox, Context, ViewportBuilder, ViewportId};
use eframe::epaint::Color32;
use serde::{Deserialize, Serialize};

use crate::calendar_item_repetition::CalendarItemRepetition;
use crate::colors;
use qualified_id::QualifiedId;
use repository::{Repository, SimpleRepository};
use widgets::{SpinBox, TimePicker, TimePickerMode};
use crate::calendar_item_duration::CalendarItemDuration;
use crate::calendar_item_times::CalendarItemTimes;
use crate::role::{RoleDefinition, RoleImportance, RoleTitle, RoleType};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct AddRoleWindowState {
    show: bool,
    title: String,
    r#type: Option<QualifiedId<RoleType>>,
    date: NaiveDate,
    hour: u8,
    minute: u8,
    second: u8,
    duration_weeks: u64,
    duration_days: u64,
    duration_hours: u64,
    duration_minutes: u64,
    duration_seconds: u64,
    current_selection_mode: TimePickerMode,
    repeats: CalendarItemRepetition,
    importance: RoleImportance,
    error_message: String,
}

impl AddRoleWindowState {
    pub fn new() -> Self {
        Self {
            show: false,
            title: String::new(),
            r#type: None,
            date: Utc::now().date_naive(),
            hour: 1,
            minute: 0,
            second: 0,
            duration_weeks: 0,
            duration_days: 0,
            duration_hours: 0,
            duration_minutes: 0,
            duration_seconds: 0,
            current_selection_mode: TimePickerMode::Hour,
            repeats: CalendarItemRepetition::None,
            importance: RoleImportance::Required,
            error_message: String::new(),
        }
    }

    pub fn reset(&mut self) {
        self.show = false;
        self.title = String::new();
        self.hour = 1;
        self.minute = 0;
        self.second = 0;
        self.duration_weeks = 0;
        self.duration_days = 0;
        self.duration_hours = 0;
        self.duration_minutes = 0;
        self.duration_seconds = 0;
        self.repeats = CalendarItemRepetition::None;
        self.importance = RoleImportance::Required;
        self.error_message = String::new();
    }

    pub fn show(&mut self, day: NaiveDate) {
        self.date = day;
        self.show = true;
    }

    pub fn window(
        &mut self,
        ctx: &Context,
        mut add_role: impl FnMut(RoleDefinition) -> bool,
        role_types: &SimpleRepository<RoleType>,
    ) {
        if self.show {
            ctx.show_viewport_immediate(
                ViewportId::from_hash_of("new person window"),
                ViewportBuilder::default(),
                |context, _viewport_class| {
                    CentralPanel::default().show(context, |ui| {
                        ui.heading("Title:");
                        ui.text_edit_singleline(&mut self.title);

                        ui.heading("Type:");
                        ComboBox::from_id_salt("Type:")
                            .selected_text(match self.r#type {
                                Some(t) => match role_types.get(&t) {
                                    Some(v) => v.value(),
                                    None => String::new(),
                                },
                                None => String::new(),
                            })
                            .show_ui(ui, |ui| {
                                for role in role_types.iter() {
                                    let label_response = ui.selectable_label(
                                        Some(*role.0) == self.r#type,
                                        role.1.value(),
                                    );
                                    if label_response.clicked() {
                                        self.r#type = Some(*role.0);
                                    }
                                }
                            });

                        ui.heading("Start Time:");
                        TimePicker::new()
                            .width(150f32)
                            .hour_hand_color(Color32::from_hex("#E07400").unwrap())
                            .minute_hand_color(Color32::from_hex("#E07400").unwrap())
                            .preview_color(Color32::from_hex("#E07400").unwrap())
                            .show(ui,
                                  &mut self.hour,
                                  &mut self.minute,
                                  &mut self.current_selection_mode);

                        ui.heading("Duration:");
                        ui.horizontal(|ui| {
                            SpinBox::new()
                                .button_color(colors::HIGHLIGHT)
                                .show(ui, &mut self.duration_weeks, 0, u64::MAX, 1);
                            ui.label("w");
                            SpinBox::new()
                                .button_color(colors::HIGHLIGHT)
                                .show(ui, &mut self.duration_days, 0, 31, 1);
                            ui.label("d");
                            SpinBox::new()
                                .button_color(colors::HIGHLIGHT)
                                .show(ui, &mut self.duration_hours, 0, 24, 1);
                            ui.label("h");
                            SpinBox::new()
                                .button_color(colors::HIGHLIGHT)
                                .show(ui, &mut self.duration_minutes, 0, 60, 1);
                            ui.label("m");
                            SpinBox::new()
                                .button_color(colors::HIGHLIGHT)
                                .show(ui, &mut self.duration_seconds, 0, 60, 1);
                            ui.label("s");
                        });

                        ui.heading("Repetition:");
                        ComboBox::from_id_salt("Repetition:")
                            .selected_text(match self.repeats {
                                CalendarItemRepetition::None => "None",
                                CalendarItemRepetition::Weekly { .. } => "Weekly",
                            })
                            .show_ui(ui, |ui| {
                                let none_response = ui.selectable_label(
                                    self.repeats == CalendarItemRepetition::None,
                                    "None",
                                );
                                if none_response.clicked() {
                                    self.repeats = CalendarItemRepetition::None;
                                }

                                let weekly_response = ui.selectable_label(
                                    if let CalendarItemRepetition::Weekly { .. } = self.repeats {
                                        true
                                    } else {
                                        false
                                    },
                                    "Weekly",
                                );
                                if weekly_response.clicked() {
                                    self.repeats = CalendarItemRepetition::Weekly { count: None };
                                }
                            });

                        ui.label(self.error_message.clone());

                        ui.horizontal(|ui| {
                            let add_button = Button::new("Add");
                            let add_button_response = ui.add(add_button);
                            if add_button_response.clicked() {
                                let role = RoleDefinition::new(
                                    RoleTitle::new(self.title.clone()),
                                    // TODO remove unwrap
                                    self.r#type.unwrap(),
                                    // TODO remove unwrap
                                    CalendarItemTimes::new(
                                        self.date
                                            .and_hms_opt(
                                                self.hour as u32,
                                                self.minute as u32,
                                                self.second as u32,
                                            )
                                            .unwrap(),
                                        self.repeats,
                                    ),
                                    CalendarItemDuration::new(Duration::microseconds(
                                        (((((((self.duration_weeks * 7) + self.duration_days)
                                            * 60)
                                            + self.duration_hours)
                                            * 24
                                            + self.duration_minutes)
                                            * 60
                                            + self.duration_seconds)
                                            * 1000000)
                                            as i64,
                                    )),
                                    self.importance,
                                );
                                let added_role = add_role(role);
                                if added_role {
                                    self.reset();
                                } else {
                                    self.error_message = "role already exists".into();
                                }
                            }

                            let cancel_button = Button::new("Cancel");
                            let cancel_button_response = ui.add(cancel_button);
                            if cancel_button_response.clicked() {
                                self.reset();
                            }
                        });
                    });
                    if context.input(|input_state| input_state.viewport().close_requested()) {
                        self.reset();
                    }
                },
            );
        }
    }
}
