pub struct AlgorithmProgress {
    generations_done_percent: f32,
    roles_filled: u64,
    distribution_status: Vec<u64>,
}

impl AlgorithmProgress {
    pub fn new(
        generations_done_percent: f32,
        roles_filled: u64,
        distribution_status: Vec<u64>,
    ) -> Self {
        Self {
            generations_done_percent,
            roles_filled,
            distribution_status,
        }
    }

    pub fn generations_done_percent(&self) -> f32 {
        self.generations_done_percent
    }

    pub fn roles_filled(&self) -> u64 {
        self.roles_filled
    }

    pub fn distribution_status(&self) -> &Vec<u64> {
        &self.distribution_status
    }
}
