use crate::person_name::PersonName;
use qualified_id::QualifiedId;
use serde::{Deserialize, Serialize};
use std::collections::HashSet;
use std::hash::{Hash, Hasher};
use crate::role::RoleType;

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Person {
    name: PersonName,
    capabilities: HashSet<QualifiedId<RoleType>>,
}

impl Person {
    pub fn new<N, T>(name: N, capabilities: T) -> Self
    where
        N: Into<PersonName>,
        T: IntoIterator<Item = QualifiedId<RoleType>>,
    {
        Self {
            name: name.into(),
            capabilities: capabilities.into_iter().collect(),
        }
    }

    pub fn name(&self) -> PersonName {
        self.name.clone()
    }

    pub fn has_capability(&self, capability: &QualifiedId<RoleType>) -> bool {
        self.capabilities.contains(capability)
    }

    pub fn capabilities(&self) -> &HashSet<QualifiedId<RoleType>> {
        &self.capabilities
    }

    pub fn with_capability(&self, role_type: QualifiedId<RoleType>) -> Self {
        let mut capabilities = self.capabilities.clone();
        capabilities.insert(role_type);
        Self::new(self.name.clone(), capabilities)
    }

    pub fn without_capability(&self, role_type: &QualifiedId<RoleType>) -> Self {
        let mut capabilities = self.capabilities.clone();
        capabilities.remove(role_type);
        Self::new(self.name.clone(), capabilities)
    }

    pub fn with_name(&self, name: String) -> Self {
        Self::new(name, self.capabilities.clone())
    }
}

impl Hash for Person {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.name.hash(state);
    }
}

impl From<&Person> for Person {
    fn from(value: &Person) -> Self {
        value.clone()
    }
}
