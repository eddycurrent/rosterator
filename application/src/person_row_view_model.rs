use eframe::epaint::Color32;
use egui::{Button, Grid, Label, ScrollArea};
use egui_extras::{TableBody, TableRow};
use itertools::Itertools;
use serde::{Deserialize, Serialize};

use qualified_id::QualifiedId;
use repository::{Repository, SimpleRepository};

use crate::person::Person;
use crate::role::RoleType;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct PersonRowViewModel {
    person_id: QualifiedId<Person>,
    name_edit: Option<String>,
}

impl PersonRowViewModel {
    pub fn new(person: QualifiedId<Person>) -> Self {
        Self {
            person_id: person,
            name_edit: None,
        }
    }

    pub fn person_id(&self) -> QualifiedId<Person> {
        self.person_id
    }

    pub fn with_edit(&self, name_edit: Option<String>) -> Self {
        Self {
            person_id: self.person_id.clone(),
            name_edit,
        }
    }

    fn render_name(&self, row: &mut TableRow, person: &mut Person) -> Option<String> {
        let mut output = self.name_edit.clone();
        row.col(|ui| match &self.name_edit {
            Some(value) => {
                let mut value = value.clone();
                ui.text_edit_singleline(&mut value);
                ui.horizontal(|ui| {
                    let save_button = Button::new("Save");
                    let save_button_response = ui.add(save_button);
                    let cancel_button = Button::new("Cancel");
                    let cancel_button_response = ui.add(cancel_button);
                    if save_button_response.clicked() {
                        *person = person.with_name(value);
                        output = None;
                    } else if cancel_button_response.clicked() {
                        output = None;
                    } else {
                        output = Some(value)
                    }
                });
            }
            None => {
                ScrollArea::vertical().show(ui, |ui| {
                    let role_type_description_label = Label::new(person.name());
                    let person_name_label_response = ui.add(role_type_description_label);
                    if person_name_label_response.clicked() {
                        output = Some(person.name().value());
                    }
                });
            }
        });
        output
    }

    pub fn render(
        &self,
        table_body: &mut TableBody,
        people: &mut SimpleRepository<Person>,
        role_types: &SimpleRepository<RoleType>,
    ) -> Option<Self> {
        people.get_mut(&self.person_id).and_then(|person| {
            let mut output = Some(self.clone());
            table_body.row(50.0, |mut row| {
                output = Some(self.with_edit(self.render_name(&mut row, person)));

                row.col(|ui| {
                    ScrollArea::vertical()
                        .max_width(ui.available_width())
                        .show(ui, |ui| {
                            Grid::new(person.name()).show(ui, |ui| {
                                for role_type_chunk in &role_types.iter().chunks(4) {
                                    for role_type in role_type_chunk {
                                        let mut checked =
                                            person.capabilities().contains(role_type.0);
                                        let original_value = checked;
                                        ui.checkbox(&mut checked, role_type.1.value());
                                        if checked != original_value {
                                            if checked {
                                                *person =
                                                    person.with_capability(role_type.0.clone());
                                            } else {
                                                *person = person.without_capability(role_type.0);
                                            }
                                        }
                                    }
                                    ui.end_row();
                                }
                            });
                        });
                });
                row.col(|ui| {
                    let delete_button = Button::new("Delete").fill(Color32::DARK_RED);
                    let delete_button_response = ui.add(delete_button);
                    if delete_button_response.clicked() {
                        output = None;
                    }
                });
            });
            output
        })
    }
}
