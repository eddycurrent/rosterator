use chrono::{Months, NaiveDateTime, Utc};
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};

use crate::availability_definition::AvailabilityDefinition;
use crate::person::Person;
use crate::roster::Roster;
use repository::SimpleRepository;
use crate::role::{RoleDefinition, RoleType};

#[derive(Clone, Serialize, Deserialize)]
pub struct Project {
    pub path: Option<String>,
    #[serde(default)]
    pub role_types: SimpleRepository<RoleType>,
    #[serde(default)]
    pub role_definitions: HashSet<RoleDefinition>,
    #[serde(default)]
    pub people: SimpleRepository<Person>,
    #[serde(default)]
    pub availability_definitions: HashSet<AvailabilityDefinition>,
    pub schedule_from: NaiveDateTime,
    pub schedule_to: NaiveDateTime,
    #[serde(default)]
    pub saved_rosters: HashMap<String, Roster>,
}

impl Project {
    pub fn new_from_components(
        path: Option<String>,
        role_types: SimpleRepository<RoleType>,
        role_definitions: HashSet<RoleDefinition>,
        people: SimpleRepository<Person>,
        availability_definitions: HashSet<AvailabilityDefinition>,
        schedule_from: NaiveDateTime,
        schedule_to: NaiveDateTime,
        saved_rosters: HashMap<String, Roster>,
    ) -> Self {
        Self {
            path,
            role_types,
            role_definitions,
            people,
            availability_definitions,
            schedule_from,
            schedule_to,
            saved_rosters,
        }
    }

    pub fn empty() -> Self {
        Self::new_from_components(
            None,
            SimpleRepository::new(),
            HashSet::new(),
            SimpleRepository::new(),
            HashSet::new(),
            Utc::now().naive_utc(),
            Utc::now().naive_utc() + Months::new(1),
            HashMap::new(),
        )
    }
}
