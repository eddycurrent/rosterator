use chrono::NaiveDateTime;

pub trait GeneratorStopPredicate<T>
where
    T: PartialOrd,
{
    fn stop(&self, value: &T) -> bool;
}

impl GeneratorStopPredicate<NaiveDateTime> for NaiveDateTime {
    fn stop(&self, value: &NaiveDateTime) -> bool {
        self >= value
    }
}
