use crate::assignment::Assignment;
use crate::main_configuration::MainConfiguration;
use itertools::Itertools;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct Roster {
    assignments: Vec<Assignment>,
    consecutive_counts: Vec<u64>,
}

impl Roster {
    pub fn new(assignments: Vec<Assignment>, main_configuration: &MainConfiguration) -> Self {
        let consecutive_counts =
            Self::calculate_consecutive_counts(&assignments, main_configuration);
        Self {
            assignments,
            consecutive_counts,
        }
    }

    pub fn assignments(&self) -> &Vec<Assignment> {
        &self.assignments
    }

    pub fn consecutive_counts(&self) -> &Vec<u64> {
        &self.consecutive_counts
    }

    pub fn calculate_consecutive_counts(
        assignments: &Vec<Assignment>,
        main_configuration: &MainConfiguration,
    ) -> Vec<u64> {
        let people_per_role = main_configuration.possible_people_per_role_type();
        let role_types = assignments
            .iter()
            .map(|assignment| assignment.role().r#type())
            .unique()
            .collect_vec();

        let mut output = vec![0_u64; 6];

        let assignments_by_role_type = assignments
            .iter()
            .into_group_map_by(|assignment| assignment.role().r#type());

        for role_type in role_types {
            if people_per_role[&role_type] > 2 {
                output[0] += assignments_by_role_type[&role_type]
                    .iter()
                    .tuple_windows::<(_, _)>()
                    .filter(|tuple| tuple.0.person() == tuple.1.person())
                    .count() as u64;
            }
            if people_per_role[&role_type] > 3 {
                output[1] += assignments_by_role_type[&role_type]
                    .iter()
                    .tuple_windows::<(_, _, _)>()
                    .filter(|tuple| tuple.0.person() == tuple.2.person())
                    .count() as u64;
            }
            if people_per_role[&role_type] > 4 {
                output[2] += assignments_by_role_type[&role_type]
                    .iter()
                    .tuple_windows::<(_, _, _, _)>()
                    .filter(|tuple| tuple.0.person() == tuple.3.person())
                    .count() as u64;
            }
            if people_per_role[&role_type] > 5 {
                output[3] += assignments_by_role_type[&role_type]
                    .iter()
                    .tuple_windows::<(_, _, _, _, _)>()
                    .filter(|tuple| tuple.0.person() == tuple.4.person())
                    .count() as u64;
            }
            if people_per_role[&role_type] > 6 {
                output[4] += assignments_by_role_type[&role_type]
                    .iter()
                    .tuple_windows::<(_, _, _, _, _, _)>()
                    .filter(|tuple| tuple.0.person() == tuple.5.person())
                    .count() as u64;
            }
            if people_per_role[&role_type] > 7 {
                output[5] += assignments_by_role_type[&role_type]
                    .iter()
                    .tuple_windows::<(_, _, _, _, _, _, _)>()
                    .filter(|tuple| tuple.0.person() == tuple.6.person())
                    .count() as u64;
            }
        }

        output
    }
}
