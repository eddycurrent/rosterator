use std::cmp::Ordering;

use chrono::{NaiveDate, NaiveDateTime};
use serde::{Deserialize, Serialize};

use qualified_id::QualifiedId;
use crate::calendar_item_duration::CalendarItemDuration;
use crate::generator_predicate::GeneratorStopPredicate;
use crate::person::Person;
use crate::person_name::PersonName;

#[derive(Clone, Debug, PartialEq, Eq, Serialize, Deserialize)]
pub struct Availability {
    person_name: PersonName,
    person: QualifiedId<Person>,
    time: NaiveDateTime,
    duration: CalendarItemDuration,
}

impl Availability {
    pub fn new(person_name: PersonName, person: QualifiedId<Person>, time: NaiveDateTime, duration: CalendarItemDuration) -> Self {
        Self {
            person_name,
            person,
            time,
            duration,
        }
    }

    pub fn person_name(&self) -> PersonName {
        self.person_name.clone()
    }

    pub fn person(&self) -> QualifiedId<Person> {
        self.person
    }

    pub fn time(&self) -> NaiveDateTime {
        self.time
    }

    pub fn duration(&self) -> CalendarItemDuration {
        self.duration
    }
}

impl PartialOrd for Availability {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        self.time.time().partial_cmp(&other.time.time())
    }
}

impl Ord for Availability {
    fn cmp(&self, other: &Self) -> Ordering {
        self.time.time().cmp(&other.time.time())
    }
}

impl GeneratorStopPredicate<NaiveDate> for Availability {
    fn stop(&self, value: &NaiveDate) -> bool {
        self.time.date() >= *value
    }
}
