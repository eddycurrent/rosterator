#![feature(map_try_insert)]

mod repository;
pub use repository::Repository;

mod simple_repository;
pub use simple_repository::SimpleRepository;
