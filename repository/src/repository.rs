use std::hash::Hash;

use qualified_id::QualifiedId;

pub trait Repository<T> where T: Eq + Hash {
    fn add(&mut self, value: T) -> QualifiedId<T>;
    
    fn get(&self, id: &QualifiedId<T>) -> Option<&T>;
    
    fn get_mut(&mut self, id: &QualifiedId<T>) -> Option<&mut T>;
    
    fn update(&mut self, id: QualifiedId<T>, value: T);
    
    fn remove(&mut self, id: &QualifiedId<T>) -> Option<T>;
}