use std::collections::hash_map::Iter;
use std::hash::Hash;

use serde::{Deserialize, Serialize};
use bidirectional_hash_map::BidirectionalHashMap;

use qualified_id::QualifiedId;

use crate::Repository;

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct SimpleRepository<T> where T: Eq + Hash + Clone {
    values: BidirectionalHashMap<QualifiedId<T>, T>,
}

impl<T> SimpleRepository<T> where T: Eq + Hash + Clone {
    pub fn new() -> Self {
        Self {
            values: BidirectionalHashMap::new(),
        }
    }
    
    pub fn iter(&self) -> Iter<QualifiedId<T>, T> {
        self.values.a_to_b().iter()
    }
    
    pub fn keys(&self) -> Vec<QualifiedId<T>> {
        self.values.a_to_b().keys().copied().collect()
    }
    
    pub fn values(&self) -> Vec<T> {
        self.values.a_to_b().values().cloned().collect()
    }
}

impl<T> Repository<T> for SimpleRepository<T> where T: Eq + Hash + Clone {
    fn add(&mut self, value: T) -> QualifiedId<T> {
        match self.values.contains_a_b(&value) {
            true => self.values.b_to_a()[&value],
            false => {
                let id = QualifiedId::new();
                self.values.insert(id, value);
                id
            }
        }
    }

    fn get(&self, id: &QualifiedId<T>) -> Option<&T> {
        self.values.get_a_to_b(id)
    }

    fn get_mut(&mut self, id: &QualifiedId<T>) -> Option<&mut T> {
        self.values.get_mut_a_to_b(id)
    }

    fn update(&mut self, id: QualifiedId<T>, value: T) {
        self.values.insert(id, value);
    }

    fn remove(&mut self, id: &QualifiedId<T>) -> Option<T> {
        self.values.remove_by_a_key(id)
    }
}

impl<T> Default for SimpleRepository<T> where T: Eq + Hash + Clone {
    fn default() -> Self {
        Self::new()
    }
}
